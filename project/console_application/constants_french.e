note
	description: "French text constants."
	author: "Louis Marchand"
	date: "2014, november 5"
	revision: "0.1"

class
	CONSTANTS_FRENCH

inherit
	CONSTANTS
		redefine
			true_text, false_text, error_syntax_not_valid,
			error_expression_not_valid, error_variable_not_found,
			error_function_not_found, error_numeric_not_valid,
			error_boolean_not_valid, error_cannot_compare,
			error_incorrect_value, error_usage_prefix,
			error_internal_error, error_module_name_not_valid,
			error_module_name_already_in_use, error_general,
			error_main_module_duplicated, error_usage_sufix,
			void_text, concatenation, error_no_main_module,
			create_list, error_list_index_not_valid, close_file,
			error_operator_used_on_void_variable, create_file,
			error_argument_count, error_function_name_duplicate,
			error_list_not_valid, file_text, line_text, column_text,
			error_module_not_found, error_function_used_in_procedure_call,
			error_module_argument_count_not_valid, error_file_not_valid,
			error_file_path_not_valid, error_command_name_not_valid,
			error_arguments_not_valid, language, error_general_in_file,
			error_main_module_not_found, error_source_file_not_valid
		end

feature -- Access

	language:READABLE_STRING_GENERAL
			-- <Precursor>
		do
			Result := "fran�ais"
		end

feature -- Value Constants


	true_text:READABLE_STRING_GENERAL
			-- <Precursor>
		do
			Result := "Vrai"
		end

	false_text:READABLE_STRING_GENERAL
			-- <Precursor>
		do
			Result := "Faux"
		end

	void_text:READABLE_STRING_GENERAL
			-- <Precursor>
		do
			Result := "Vide"
		end

	line_text:READABLE_STRING_GENERAL
			-- <Precursor>
		do
			Result := "Ligne"
		end

feature -- Error manager constants

	column_text:READABLE_STRING_GENERAL
			-- <Precursor>
		do
			Result := "Colonne"
		end

	file_text:READABLE_STRING_GENERAL
			-- <Precursor>
		do
			Result := "Fichier"
		end

	error_usage_prefix:READABLE_STRING_GENERAL
			-- <Precursor>
		do
			Result := "Utilisation: "
		end

	error_usage_sufix:READABLE_STRING_GENERAL
			-- <Precursor>
		do
			Result := " <fichier_source>"
		end

	error_general:READABLE_STRING_GENERAL
			-- <Precursor>
		do
			Result := "Une erreur est survenue"
		end

	error_general_in_file:READABLE_STRING_GENERAL
			-- <Precursor>
		do
			Result := "Une erreur est survenue dans le fichier "
		end

	error_source_file_not_valid:READABLE_STRING_GENERAL
			-- <Precursor>
		do
			Result := "Le fichier n'est pas lisible: "
		end

	error_internal_error:READABLE_STRING_GENERAL
			-- <Preucrsor>
		do
			Result := "Erreur interne d�tect�."
		end

	error_syntax_not_valid:READABLE_STRING_GENERAL
			-- <Precursor>
		do
			Result := "Syntaxe invalide"
		end

	error_module_name_not_valid:READABLE_STRING_GENERAL
			-- <Preucrsor>
		do
			Result := "Nom de module invalide"
		end

	error_module_name_already_in_use:READABLE_STRING_GENERAL
			-- <Precursor>
		do
			Result := "Nom de module d�j� utilis�"
		end

	error_main_module_duplicated:READABLE_STRING_GENERAL
			-- <Precursor>
		do
			Result := "Module principal d�j� trouv�"
		end

	error_main_module_not_found:READABLE_STRING_GENERAL
			-- <Precursor>
		do
			Result := "Main module not found"
		end

	error_expression_not_valid:READABLE_STRING_GENERAL
			-- <Precursor>
		do
			Result := "Expression invalide"
		end

	error_variable_not_found:READABLE_STRING_GENERAL
			-- <Precursor>
		do
			Result := "Variable innexistante"
		end

	error_function_not_found:READABLE_STRING_GENERAL
			-- <Precursor>
		do
			Result := "Function innexistante"
		end

	error_numeric_not_valid:READABLE_STRING_GENERAL
			-- <Precursor>
		do
			Result := "Valeur num�rique invalide"
		end

	error_boolean_not_valid:READABLE_STRING_GENERAL
			-- <Precursor>
		do
			Result := "Valeur bool�enne invalide"
		end

	error_cannot_compare:READABLE_STRING_GENERAL
			-- <Precursor>
		do
			Result := "Ne peut comparer ces types"
		end

	error_incorrect_value:READABLE_STRING_GENERAL
			-- <Precursor>
		do
			Result := "Valeur erron�e"
		end

	error_no_main_module:READABLE_STRING_GENERAL
			-- <Precursor>
		do
			Result := "Le programme ne contient aucun module principal."
		end

	error_list_index_not_valid:READABLE_STRING_GENERAL
			-- <Precursor>
		do
			Result := "L'index de liste n'est pas valide."
		end

	error_operator_used_on_void_variable:READABLE_STRING_GENERAL
			-- <Precursor>
		do
			Result := "Impossible d'utiliser un op�rateur sur une variable Vide"
		end

	error_argument_count:READABLE_STRING_GENERAL
			-- <Precursor>
		do
			Result := "Nombre incorrecte d'argument lors de l'appel d'un module."
		end

	error_function_name_duplicate:READABLE_STRING_GENERAL
			-- <Precursor>
		do
			Result := "Le m�me nom de fonction a �t� utilis� plusieurs fois."
		end

	error_list_not_valid:READABLE_STRING_GENERAL
			-- <Precursor>
		do
			Result := "La variable n'est pas une liste valide."
		end

	error_module_not_found:READABLE_STRING_GENERAL
			-- <Precursor>
		do
			Result := "Le module n'existe pas."
		end

	error_function_used_in_procedure_call:READABLE_STRING_GENERAL
			-- <Precursor>
		do
			Result := "Fonction utilis� dans un appel de proc�dure."
		end

	error_module_argument_count_not_valid:READABLE_STRING_GENERAL
			-- <Precursor>
		do
			Result := "Nombre d'argument incorrect."
		end

	error_file_not_valid:READABLE_STRING_GENERAL
			-- <Precursor>
		do
			Result := "Fichier non valide."
		end

	error_file_path_not_valid:READABLE_STRING_GENERAL
			-- <Precursor>
		do
			Result := "Chemin de fichier non valide"
		end

	error_command_name_not_valid:READABLE_STRING_GENERAL
			-- <Precursor>
		do
			Result := "La commande n'est pas valide."
		end

	error_arguments_not_valid:READABLE_STRING_GENERAL
			-- <Precursor>
		do
			Result := "Un argument n'est pas valide."
		end

feature -- Function names

	concatenation:READABLE_STRING_GENERAL
			-- <Precursor>
		do
			Result := "concat�ner"
		end

	create_list:READABLE_STRING_GENERAL
			-- <Precursor>
		do
			Result := "cr�er_liste"
		end

	create_file:READABLE_STRING_GENERAL
			-- <Precursor>
		do
			Result := "cr�er_fichier"
		end

feature -- Commands names

	close_file:READABLE_STRING_GENERAL
			-- <Precursor>
		do
			Result := "fermer_fichier"
		end
end
