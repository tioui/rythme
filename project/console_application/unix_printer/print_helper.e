note
	description: "Wrap the UTF-8 conversion of the print for UNIX console"
	author: "Louis Marchand"
	date: "2014, november 6"
	revision: "1.0"

deferred class
	PRINT_HELPER

feature {NONE} -- Access

	print_standard(a_message:READABLE_STRING_GENERAL)
			-- Standard output method
		do
			io.standard_default.put_string (converter.string_32_to_utf_8_string_8 (a_message.to_string_32))
		end

	print_error(a_message:READABLE_STRING_GENERAL)
			-- Error output method
		do
			io.error.put_string (converter.string_32_to_utf_8_string_8 (a_message.to_string_32))
		end

	converter:UTF_CONVERTER
			-- The Eiffel {STRING_GENERAL} to UTF converter
		once
			create Result
		end

end
