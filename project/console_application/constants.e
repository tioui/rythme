note
	description: "Base (English) text constants."
	author: "Louis Marchand"
	date: "2014, november 5"
	revision: "0.1"

class
	CONSTANTS

feature -- Access

	language:READABLE_STRING_GENERAL
			-- A text representation of the language represented by `Current'
		do
			Result := "english"
		end

feature -- Value Constants

	true_text:READABLE_STRING_GENERAL
			-- Text representation of a True Value
		do
			Result := "True"
		end

	false_text:READABLE_STRING_GENERAL
			-- Text representation of a False Value
		do
			Result := "False"
		end

	file_text:READABLE_STRING_GENERAL
			-- Text representation of a File
		do
			Result := "File"
		end

	void_text:READABLE_STRING_GENERAL
			-- Text representation of a Void variable
		do
			Result := "Void"
		end

feature -- Error manager constants

	line_text:READABLE_STRING_GENERAL
			-- Text or the word "Line"
		do
			Result := "Line"
		end

	column_text:READABLE_STRING_GENERAL
			-- Text or the word "Column"
		do
			Result := "Column"
		end

	error_usage_prefix:READABLE_STRING_GENERAL
			-- Prefix of the text to print when there is a usage error
		do
			Result := "Usage: "
		end

	error_usage_sufix:READABLE_STRING_GENERAL
			-- Sufix of the text to print when there is a usage error
		do
			Result := " <source_file>"
		end

	error_general:READABLE_STRING_GENERAL
			-- Text to print when every error occured
		do
			Result := "An error occured"
		end

	error_general_in_file:READABLE_STRING_GENERAL
			-- Text to print when every error occured with known file
		do
			Result := "An error occured in file "
		end

	error_source_file_not_valid:READABLE_STRING_GENERAL
			-- Text to print when a file is not readable or does not exist
		do
			Result := "Cannot read file "
		end

	error_internal_error:READABLE_STRING_GENERAL
			-- Text to print when an internal error occured
		do
			Result := "Internal error detected. This is a bug."
		end

	error_module_name_not_valid:READABLE_STRING_GENERAL
			-- Text to show on an invalid module name error
		do
			Result := "Module name not valid"
		end

	error_module_name_already_in_use:READABLE_STRING_GENERAL
			-- Text to show on a duplcated module name error
		do
			Result := "Module name already in use"
		end

	error_main_module_duplicated:READABLE_STRING_GENERAL
			-- Text to show on a duplcated main module error
		do
			Result := "Main module duplicated"
		end

	error_main_module_not_found:READABLE_STRING_GENERAL
			-- Text to show on no main module module error
		do
			Result := "Main module not found"
		end

	error_syntax_not_valid:READABLE_STRING_GENERAL
			-- Text to show on a syntax error
		do
			Result := "Syntax not valid"
		end

	error_expression_not_valid:READABLE_STRING_GENERAL
			-- Text to show on an expression not valid error
		do
			Result := "Expression_not_valid"
		end

	error_variable_not_found:READABLE_STRING_GENERAL
			-- Text to show on a variable not found error
		do
			Result := "Variable not found"
		end

	error_function_not_found:READABLE_STRING_GENERAL
			-- Text to show on a function not found error
		do
			Result := "Function not found"
		end

	error_numeric_not_valid:READABLE_STRING_GENERAL
			-- Text to show on a numeric value not valid error
		do
			Result := "Not a valid numeric value"
		end

	error_boolean_not_valid:READABLE_STRING_GENERAL
			-- Text to show on a boolean value not valid error
		do
			Result := "Not a valid boolean value"
		end

	error_cannot_compare:READABLE_STRING_GENERAL
			-- Text to show on a objects cannot be compare error
		do
			Result := "Cannot compare incompatible value"
		end

	error_incorrect_value:READABLE_STRING_GENERAL
			-- Text to show on an incorrect value error
		do
			Result := "Incorrect value"
		end

	error_no_main_module:READABLE_STRING_GENERAL
			-- Text to show when the program does not have a main module
		do
			Result := "The program does not have a main module."
		end

	error_list_index_not_valid:READABLE_STRING_GENERAL
			-- Text to show when the program used an invalid list index
		do
			Result := "The list index is not valid."
		end

	error_operator_used_on_void_variable:READABLE_STRING_GENERAL
			-- Text to show when the program used an operator on a variable that has no value
		do
			Result := "Cannot use operator on Void variable."
		end

	error_argument_count:READABLE_STRING_GENERAL
			-- Text to show when the program used an invalid number of argument for a module
		do
			Result := "Wrong number of arguments on moule call."
		end

	error_function_name_duplicate:READABLE_STRING_GENERAL
			-- Text to show when the program duplicated a function name
		do
			Result := "A function name has been duplicated."
		end

	error_list_not_valid:READABLE_STRING_GENERAL
			-- Text to show when an invalid list name is used
		do
			Result := "The variable is not a valid list."
		end

	error_module_not_found:READABLE_STRING_GENERAL
			-- Text to show when aa module call is made but the module does not exist
		do
			Result := "The module does not exist."
		end

	error_function_used_in_procedure_call:READABLE_STRING_GENERAL
			-- Text to show when a function is used in a procedure call
		do
			Result := "Function used in procedure call."
		end

	error_module_argument_count_not_valid:READABLE_STRING_GENERAL
			-- Text to show when an invalid module argument count is use in module call
		do
			Result := "Invalid number of arguments."
		end

	error_file_not_valid:READABLE_STRING_GENERAL
			-- Text to show when an invalid file has been used
		do
			Result := "Invalid file."
		end

	error_file_path_not_valid:READABLE_STRING_GENERAL
			-- Text to show when an invalid file path has been used
		do
			Result := "File path not valid."
		end

	error_command_name_not_valid:READABLE_STRING_GENERAL
			-- Text to show when an invalid command name has been used
		do
			Result := "The command name is not valid."
		end

	error_arguments_not_valid:READABLE_STRING_GENERAL
			-- Text to show when a command or a function has been used with an invalid argument
		do
			Result := "An argument is not valid."
		end

feature -- Function names

	concatenation:READABLE_STRING_GENERAL
			-- The concatenation function name
		do
			Result := "concatenation"
		end

	create_list:READABLE_STRING_GENERAL
			-- The list creation function name
		do
			Result := "create_list"
		end

	create_file:READABLE_STRING_GENERAL
			-- The file creation function name
		do
			Result := "create_file"
		end

feature -- Commands name

	close_file:READABLE_STRING_GENERAL
			-- The closing file command name
		do
			Result := "close_file"
		end

end
