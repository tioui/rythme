note
	description: "Base error system for the Rythme parser and interpreter."
	author: "Louis Marchand"
	date: "2014, november 10"
	revision: "0.1"

deferred class
	RYTHME_ERROR

inherit
	ANY
		redefine
			default_create
		end

feature {NONE} -- Initilization

	default_create
			-- Initialization or `Current'
		do
			clear
		end

feature -- Access

	clear
			-- Put `Current' in a state of "No error"
		do
			syntax_not_valid := False
			internal_error := False

			erroneous_value := Void
			line := 0
			column := 0
		ensure
			Clear_Error_Not_Exist: not exist
		end

	exist:BOOLEAN
			-- Is there an error in pending
		do
			Result :=
					syntax_not_valid or
					internal_error
		end

	internal_error:BOOLEAN
			-- `Current' represent an internal error.
			-- Should never happend! This is a bug!

	set_internal_error_with_position(a_line, a_column:INTEGER)
			-- Set `Current' as an internal error.
			-- The error occured at the line `a_line'
			-- and character `a_column' in the source input.
			-- Should never happend! This is a bug!
		require
			Should_Never_Happend: False
		do
			line := a_line
			column := a_column
			internal_error := True
		end

	set_internal_error
			-- Set `Current' as an internal error.
			-- Should never happend! This is a bug!
		require
			Should_Never_Happend: False
		do
			line := 0
			column := 0
			internal_error := True
		end

	syntax_not_valid:BOOLEAN
			-- `Current' represent an error in the code syntax

	set_syntax_not_valid(a_line, a_column:INTEGER)
			-- set `Current' as an error in the code syntax
			-- The error happend at the position indicate by `a_line' and `a_column'
			-- in the source code.
		do
			line := a_line
			column := a_column
			syntax_not_valid := True
		end

	set_source(a_source:READABLE_STRING_GENERAL)
			-- Assing `source' to `a_source'.
		do
			source := a_source
		end

	line:INTEGER
			-- Line in the source code of the error in pending

	column:INTEGER
			-- Position of the character in the `line' in the source code
			-- of the error in pending

	erroneous_value:detachable READABLE_STRING_GENERAL
			-- Value that is not recognised

	source:detachable READABLE_STRING_GENERAL
			-- Source code (file name or index) that `Current' happen in

end
