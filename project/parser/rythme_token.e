note
	description: "A single token of the Rythme syntax."
	author: "Louis Marchad"
	date: "2014, october 15"
	revision: "0.2"

class
	RYTHME_TOKEN

create
	make

feature {NONE} -- Initialization

	make(a_code, a_column, a_count, a_line:INTEGER; a_text, a_source:READABLE_STRING_GENERAL)
			-- Initialization of `Current' using `a_code' as token code found
			-- in the line `a_line' at the character position `a_column'. `Current'
			-- has a lengh of `a_count' and is represented by the text `a_text'.
		require
			Code_Is_Valid: a_code >= 0
			Line_Is_Valid: a_line >= 0
			Column_Is_Valid: a_column >= 0
			Count_Is_Valid: a_count >= 0
		do
			set_text(a_text)
			set_line (a_line)
			set_code (a_code)
			set_column (a_column)
			set_count (a_count)
			source := a_source
		ensure
			Code_Assigned: code = a_code
			Line_Assigned: line = a_line
			Column_Assigned: column = a_column
			Count_Assigned: count = a_count
			Text_Assigned: text = a_text
		end

feature -- Access

	source:READABLE_STRING_GENERAL
			-- The source code (file name or index) that `Current' has been found in

	code:INTEGER assign set_code
			-- The internal code of `Current'. See: {RYTHME_TOKENS}

	set_code(a_code:INTEGER)
			-- Assign `code' to the `a_code' value
		require
			Code_Is_Valid: a_code >= 0
		do
			code := a_code
		ensure
			Code_Assign: code = a_code
			Line_Not_Changed: line = old line
			Column_Not_Changed: column = old column
			Count_Not_Changed: count = old count
			Text_Not_Changed: text = old text
		end

	line:INTEGER assign set_line
			-- The index of the line containing `Current'

	set_line(a_line:INTEGER)
			-- Assign `line' to the `a_line' value
		require
			Line_Is_Valid: a_line >= 0
		do
			line := a_line
		ensure
			Code_Not_Changed: code = old code
			Line_Assign: line = a_line
			Column_Not_Changed: column = old column
			Count_Not_Changed: count = old count
			Text_Not_Changed: text = old text
		end

	column:INTEGER assign set_column
			-- The starting position of `Current' in the line

	set_column(a_column:INTEGER)
			-- Assign `column' to the `a_column' value
		require
			Column_Is_Valid: a_column >= 0
		do
			column := a_column
		ensure
			Code_Not_Changed: code = old code
			Line_Not_Changed: line = old line
			Column_Assign: column = a_column
			Count_Not_Changed: count = old count
			Text_Not_Changed: text = old text
		end

	count:INTEGER assign set_count
			-- The number of character of `Current'

	set_count(a_count:INTEGER)
			-- Assign `count' to the `a_count' value
		require
			Count_Is_Valid: a_count >= 0
		do
			count := a_count
		ensure
			Code_Not_Changed: code = old code
			Line_Not_Changed: line = old line
			Column_Not_Changed: column = old column
			Count_Assign: count = a_count
			Text_Not_Changed: text = old text
		end

	text:READABLE_STRING_GENERAL assign set_text
			-- The text representation of `Current' (as seen in the source input)

	set_text(a_text:READABLE_STRING_GENERAL)
			-- Assing the `text' representation of `Current' to `a_text'
		do
			text := a_text
		ensure
			Code_Not_Changed: code = old code
			Line_Not_Changed: line = old line
			Column_Not_Changed: column = old column
			Count_Not_Changed: count = old count
			Text_Assign: text = a_text
		end

	is_comma: BOOLEAN
			-- Is `Current' representing a comma
		do
			Result := code = {RYTHME_TOKENS}.TR_COMMA
		end

	is_assignment : BOOLEAN
			-- Is `Current' representing an assignment
		do
			Result := code = {RYTHME_TOKENS}.TR_ASSIGNMENT
		end

	is_eq : BOOLEAN
			-- Is `Current' representing an equal
		do
			Result := code = {RYTHME_TOKENS}.TR_EQ
		end

	is_lt : BOOLEAN
			-- Is `Current' representing a lower
		do
			Result := code = {RYTHME_TOKENS}.TR_LT
		end

	is_gt : BOOLEAN
			-- Is `Current' representing a greater
		do
			Result := code = {RYTHME_TOKENS}.TR_GT
		end

	is_le : BOOLEAN
			-- Is `Current' representing a lower and equal
		do
			Result := code = {RYTHME_TOKENS}.TR_LE
		end

	is_ge : BOOLEAN
			-- Is `Current' representing a greater and equal
		do
			Result := code = {RYTHME_TOKENS}.TR_GE
		end

	is_ne : BOOLEAN
			-- Is `Current' representing a not equal
		do
			Result := code = {RYTHME_TOKENS}.TR_NE
		end

	is_lparan : BOOLEAN
			-- Is `Current' representing a lparan
		do
			Result := code = {RYTHME_TOKENS}.TR_LPARAN
		end

	is_rparan : BOOLEAN
			-- Is `Current' representing a rparan
		do
			Result := code = {RYTHME_TOKENS}.TR_RPARAN
		end

	is_lsqure : BOOLEAN
			-- Is `Current' representing a lsqure
		do
			Result := code = {RYTHME_TOKENS}.TR_LSQURE
		end

	is_rsqure : BOOLEAN
			-- Is `Current' representing a rsqure
		do
			Result := code = {RYTHME_TOKENS}.TR_RSQURE
		end

	is_plus : BOOLEAN
			-- Is `Current' representing a plus
		do
			Result := code = {RYTHME_TOKENS}.TR_PLUS
		end

	is_minus : BOOLEAN
			-- Is `Current' representing a minus
		do
			Result := code = {RYTHME_TOKENS}.TR_MINUS
		end

	is_star : BOOLEAN
			-- Is `Current' representing a star
		do
			Result := code = {RYTHME_TOKENS}.TR_STAR
		end

	is_slash : BOOLEAN
			-- Is `Current' representing a slash
		do
			Result := code = {RYTHME_TOKENS}.TR_SLASH
		end

	is_power : BOOLEAN
			-- Is `Current' representing a power
		do
			Result := code = {RYTHME_TOKENS}.TR_POWER
		end

	is_lshift : BOOLEAN
			-- Is `Current' representing a left shift (<<)
		do
			Result := code = {RYTHME_TOKENS}.TR_LSHIFT
		end

	is_rshift : BOOLEAN
			-- Is `Current' representing a right shift (>>)
		do
			Result := code = {RYTHME_TOKENS}.TR_RSHIFT
		end

	is_lbrace : BOOLEAN
			-- Is `Current' representing a left brace {
		do
			Result := code = {RYTHME_TOKENS}.TR_LBRACE
		end

	is_rbrace : BOOLEAN
			-- Is `Current' representing a right brace }
		do
			Result := code = {RYTHME_TOKENS}.TR_RBRACE
		end

	is_div : BOOLEAN
			-- Is `Current' representing a div
		do
			Result := code = {RYTHME_TOKENS}.TR_DIV
		end

	is_mod : BOOLEAN
			-- Is `Current' representing a mod
		do
			Result := code = {RYTHME_TOKENS}.TR_MOD
		end

	is_free : BOOLEAN
			-- Is `Current' representing a free
		do
			Result := code = {RYTHME_TOKENS}.TR_FREE
		end

	is_id : BOOLEAN
			-- Is `Current' representing an id
		do
			Result := code = {RYTHME_TOKENS}.TR_ID
		end

	is_integer : BOOLEAN
			-- Is `Current' representing an integer
		do
			Result := code = {RYTHME_TOKENS}.TR_INTEGER
		end

	is_real : BOOLEAN
			-- Is `Current' representing a real
		do
			Result := code = {RYTHME_TOKENS}.TR_REAL
		end

	is_string : BOOLEAN
			-- Is `Current' representing a string
		do
			Result := code = {RYTHME_TOKENS}.TR_STRING
		end

	is_if : BOOLEAN
			-- Is `Current' representing an if
		do
			Result := code = {RYTHME_TOKENS}.TR_IF
		end

	is_then : BOOLEAN
			-- Is `Current' representing a then
		do
			Result := code = {RYTHME_TOKENS}.TR_THEN
		end

	is_else : BOOLEAN
			-- Is `Current' representing an else
		do
			Result := code = {RYTHME_TOKENS}.TR_ELSE
		end

	is_elseif : BOOLEAN
			-- Is `Current' representing an elseif
		do
			Result := code = {RYTHME_TOKENS}.TR_ELSEIF
		end

	is_endif : BOOLEAN
			-- Is `Current' representing an endif
		do
			Result := code = {RYTHME_TOKENS}.TR_ENDIF
		end

	is_for : BOOLEAN
			-- Is `Current' representing a for
		do
			Result := code = {RYTHME_TOKENS}.TR_FOR
		end

	is_from : BOOLEAN
			-- Is `Current' representing a from
		do
			Result := code = {RYTHME_TOKENS}.TR_FROM
		end

	is_to : BOOLEAN
			-- Is `Current' representing a to
		do
			Result := code = {RYTHME_TOKENS}.TR_TO
		end

	is_do : BOOLEAN
			-- Is `Current' representing a do
		do
			Result := code = {RYTHME_TOKENS}.TR_DO
		end

	is_endfor : BOOLEAN
			-- Is `Current' representing an endfor
		do
			Result := code = {RYTHME_TOKENS}.TR_ENDFOR
		end

	is_while : BOOLEAN
			-- Is `Current' representing a while
		do
			Result := code = {RYTHME_TOKENS}.TR_WHILE
		end

	is_endwhile : BOOLEAN
			-- Is `Current' representing an endwhile
		do
			Result := code = {RYTHME_TOKENS}.TR_ENDWHILE
		end

	is_until : BOOLEAN
			-- Is `Current' representing an until
		do
			Result := code = {RYTHME_TOKENS}.TR_UNTIL
		end

	is_enduntil : BOOLEAN
			-- Is `Current' representing an enduntil
		do
			Result := code = {RYTHME_TOKENS}.TR_ENDUNTIL
		end

	is_repeat : BOOLEAN
			-- Is `Current' representing a repeat
		do
			Result := code = {RYTHME_TOKENS}.TR_REPEAT
		end

	is_endrepeat : BOOLEAN
			-- Is `Current' representing an endrepeat
		do
			Result := code = {RYTHME_TOKENS}.TR_ENDREPEAT
		end

	is_or : BOOLEAN
			-- Is `Current' representing an or
		do
			Result := code = {RYTHME_TOKENS}.TR_OR
		end

	is_and : BOOLEAN
			-- Is `Current' representing an and
		do
			Result := code = {RYTHME_TOKENS}.TR_AND
		end

	is_not : BOOLEAN
			-- Is `Current' representing a not
		do
			Result := code = {RYTHME_TOKENS}.TR_NOT
		end

	is_is : BOOLEAN
			-- Is `Current' representing an is
		do
			Result := code = {RYTHME_TOKENS}.TR_IS
		end

	is_void : BOOLEAN
			-- Is `Current' representing a void
		do
			Result := code = {RYTHME_TOKENS}.TR_VOID
		end

	is_boolean_type : BOOLEAN
			-- Is `Current' representing a boolean type indicator
		do
			Result := code = {RYTHME_TOKENS}.TR_BOOLEAN_TYPE
		end

	is_integer_type : BOOLEAN
			-- Is `Current' representing an integer type indicator
		do
			Result := code = {RYTHME_TOKENS}.TR_INTEGER_TYPE
		end

	is_real_type : BOOLEAN
			-- Is `Current' representing a real type indicator
		do
			Result := code = {RYTHME_TOKENS}.TR_REAL_TYPE
		end

	is_list_type : BOOLEAN
			-- Is `Current' representing a list type indicator
		do
			Result := code = {RYTHME_TOKENS}.TR_LIST_TYPE
		end

	is_file_type : BOOLEAN
			-- Is `Current' representing a list type indicator
		do
			Result := code = {RYTHME_TOKENS}.TR_FILE_TYPE
		end

	is_execute : BOOLEAN
			-- Is `Current' representing an execute
		do
			Result := code = {RYTHME_TOKENS}.TR_EXECUTE
		end

	is_false : BOOLEAN
			-- Is `Current' representing a false
		do
			Result := code = {RYTHME_TOKENS}.TR_FALSE
		end

	is_true : BOOLEAN
			-- Is `Current' representing a true
		do
			Result := code = {RYTHME_TOKENS}.TR_TRUE
		end

	is_procedure : BOOLEAN
			-- Is `Current' representing an enter
		do
			Result := code = {RYTHME_TOKENS}.TR_PROCEDURE
		end

	is_function : BOOLEAN
			-- Is `Current' representing an enter
		do
			Result := code = {RYTHME_TOKENS}.TR_FUNCTION
		end

	is_return : BOOLEAN
			-- Is `Current' representing a return
		do
			Result := code = {RYTHME_TOKENS}.TR_RETURN
		end

	is_xor : BOOLEAN
			-- Is `Current' representing a xor
		do
			Result := code = {RYTHME_TOKENS}.TR_XOR
		end

	is_implies : BOOLEAN
			-- Is `Current' representing an implies
		do
			Result := code = {RYTHME_TOKENS}.TR_IMPLIES
		end

	is_write : BOOLEAN
			-- Is `Current' representing a write
		do
			Result := code = {RYTHME_TOKENS}.TR_WRITE
		end

	is_read : BOOLEAN
			-- Is `Current' representing a read
		do
			Result := code = {RYTHME_TOKENS}.TR_READ
		end

	is_in : BOOLEAN
			-- Is `Current' representing an in
		do
			Result := code = {RYTHME_TOKENS}.TR_IN
		end

	is_start : BOOLEAN
			-- Is `Current' representing a start
		do
			Result := code = {RYTHME_TOKENS}.TR_START
		end

	is_end : BOOLEAN
			-- Is `Current' representing an end
		do
			Result := code = {RYTHME_TOKENS}.TR_END
		end

	is_new_line : BOOLEAN
			-- Is `Current' representing an end
		do
			Result := code = {RYTHME_TOKENS}.TR_NEWLINE
		end

end
