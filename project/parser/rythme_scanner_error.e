note
	description: "Error manager for the scanning process"
	author: "Louis Marchand"
	date: "2014, november 13"
	revision: "0.1"

class
	RYTHME_SCANNER_ERROR

inherit
	RYTHME_ERROR
		redefine
			clear, exist
		end

feature -- Access

	clear
			-- <Precursor>
		do
			Precursor {RYTHME_ERROR}
			numeric_not_valid := False
		end

	exist:BOOLEAN
			-- <Precursor>
		do
			Result :=
				Precursor {RYTHME_ERROR} or
				numeric_not_valid
		end

	numeric_not_valid:BOOLEAN
			-- `Current' represent an error in a numeric value

	set_numeric_not_valid(a_line, a_column:INTEGER)
			-- set `Current' as an error in the recognition of a numeric value.
			-- The error happend at the position indicate by `a_line' and `a_column'
			-- in the source code.
		do
			line := a_line
			column := a_column
			numeric_not_valid := True
		end
end
