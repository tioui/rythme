note
	description: "A single node of the Rythme abstract syntax tree."
	author: "Louis Marchand"
	date: "2014, october 25"
	revision: "0.1"

class
	RYTHME_AST_NODE

inherit
	ANY
		redefine
			default_create
		end

create
	default_create,
	make_from_token,
	make_statements,
	make_for_loop

feature {NONE} -- Initialization

	default_create
			-- Initialization of `Current'
		do
			create children.make(0)
		end

	make_from_token(a_token:RYTHME_TOKEN)
			-- Initialization of `Current' containing
			-- `a_token' as `token'
		do
			default_create
			token := a_token
		end

	make_statements(a_statement:RYTHME_AST_NODE; a_statements: detachable RYTHME_AST_NODE)
			-- Initialization of `Current' as a statements node containing
			-- an immediate `a_statement' and a list of others `a_statements'
			-- as default children.
		do
			default_create
			is_statements := True
			children.extend (a_statement, "statement")
			children.extend (a_statements, "statements")
			set_is_ready
		end

	make_for_loop(a_variable_name, a_base_expression, a_limit_expression, a_statements:RYTHME_AST_NODE; a_step:detachable RYTHME_AST_NODE)
			-- Initialization of `Current' as an internal "for" loop node.
			-- The loop `a_statements' will repeat while the variable identifed by
			-- `a_variable_name' is lower or equal than the value of `a_limit_expression'
		do
			default_create
			is_for_loop := True
			is_statement := True
			children.extend (a_variable_name, "variable_name")
			children.extend (a_limit_expression, "limit_expression")
			children.extend (a_base_expression, "base_expression")
			children.extend (a_statements, "statements")
			children.extend (a_step, "step")
			set_is_ready
		end



feature -- Access

	source:READABLE_STRING_GENERAL
			-- The source code (file name or index) that `Current' is associate to
		do
			Result := ""
			if attached token as la_token then
				Result := la_token.source
			else
				from
					children.start
				until
					not Result.is_empty or
					children.off
				loop
					if
						attached children.item_for_iteration as la_node and then
						not la_node.source.is_empty
					then
						Result := la_node.source
					end
				end
			end
		end

	token: detachable RYTHME_TOKEN
			-- The syntax token associate with `Current'

	children: STRING_TABLE[detachable RYTHME_AST_NODE]
			-- Every sub-tree of `Current'

	line:INTEGER
			-- In what line in the original source code
			-- is positionned `Current'
		do
			Result := -1
			if attached token as la_token then
				Result := la_token.line
			else
				from
					children.start
				until
					Result > -1 or
					children.off
				loop
					if
						attached children.item_for_iteration as la_node and then
						la_node.line > -1
					then
						Result := la_node.line
					end
				end
			end
		end

	is_ready:BOOLEAN
			-- `Current' is ready to be interpret

	set_is_ready
			-- Inform `Current' that it is ready to be interpreted
		do
			is_ready := True
		end

	is_statements:BOOLEAN
			-- Is `Current' a statements (containing multiple statement) node

	is_statement:BOOLEAN
			-- Is `Current' a statement node

	set_is_statement
			-- Set `Current' as a statement node
		do
			is_statement := True
		end

	unset_is_statement
			-- Set `Current' as a node that is not a statement
		do
			is_statement := False
		end

	is_command_call:BOOLEAN
			-- Is `Current' a system command call statement node

	set_is_command_call
			-- Set `Current' as a system command call statement node
		do
			is_command_call := True
		end

	unset_is_command_call
			-- Set `Current' as a node that is not a system command call statement
		do
			is_command_call := False
		end

	is_result:BOOLEAN
			-- Is `Current` a result expression node

	set_is_result
			-- Set `Current' as a result expression node
		require
			Result_Is_Expression: is_expression
		do
			is_result := True
		end

	unset_is_result
			-- Set `Current' as a node that is not a result expression
		do
			is_result := False
		end

	is_expression:BOOLEAN
			-- Is `Current' an expression node

	set_is_expression
			-- Set `Current' as an expression node
		do
			is_expression := True
		end

	unset_is_expression
			-- Set `Current' as a node that is not an expression
		do
			is_expression := False
		end

	is_variable:BOOLEAN
			-- Is `Current' a node containing the ID of a variable (in token)

	set_is_variable
			-- Set `Current' as a node containing the ID of a variable
		do
			is_variable := True
		end

	unset_is_variable
			-- Set `Current' as a node not containing the ID of a variable
		do
			is_variable := False
		end

	is_list:BOOLEAN
			-- Is `Current' a node containing the ID of a list (in token) and
			-- a list index in children

	set_is_list
			-- Set `Current' as a node containing the ID of a list and
			-- a list index
		do
			is_list := True
		end

	unset_is_list
			-- Set `Current' as a node not containing the ID of a list
		do
			is_list := False
		end

	is_if:BOOLEAN
			-- Is `Current' a node containing a "if" control structure.
		do
			Result := attached token as la_token and then la_token.is_if
		end

	is_else:BOOLEAN
			-- Is `Current' a node containing a "else" sub control structure.
		do
			Result := attached token as la_token and then la_token.is_else
		end

	is_elseif:BOOLEAN
			-- Is `Current' a node containing a "else" sub control structure.
		do
			Result := attached token as la_token and then la_token.is_elseif
		end

	is_while:BOOLEAN
			-- Is `Current' a node containing a "while" control structure
		do
			Result := attached token as la_token and then la_token.is_while
		end

	is_repeat:BOOLEAN
			-- Is `Current' a node containing a "repeat" control structure
		do
			Result := attached token as la_token and then la_token.is_repeat
		end

	is_until:BOOLEAN
			-- Is `Current' a node containing a "until" control structure
		do
			Result := attached token as la_token and then la_token.is_until
		end

	is_for:BOOLEAN
			-- Is `Current' a node containing a "for" control structure initialization.
		do
			Result := attached token as la_token and then la_token.is_for
		end

	is_for_loop:BOOLEAN
			-- Is `Current' a node containing a "for" loop internal control structure.
			-- The for loop is alwais included inside a "for" control sutructure intialization (see `is_for')

	is_function_call:BOOLEAN
			-- Is `Current' a node containing the ID of a function (in token)

	set_is_function_call
			-- Set `Current' as a node containing the ID of a function
		do
			is_function_call := True
		end

	unset_is_function_call
			-- Set `Current' as a node not containing the ID of a function
		do
			is_function_call := False
		end

	is_procedure_call:BOOLEAN
			-- Is `Current' represent a procedure call statement
		do
			Result := attached token as la_token and then la_token.is_execute
		end

	is_assignment:BOOLEAN
			-- Is `Current' a node representing an assignement statement
		do
			Result := attached token as la_token and then la_token.is_assignment
		end

	is_procedure:BOOLEAN
			-- Is `Current' a complete abstract syntaxic tree of a procedure
		do
			Result := attached token as la_token and then la_token.is_procedure
		end

	is_function:BOOLEAN
			-- Is `Current' a complete abstract syntaxic tree of a function
		do
			Result := attached token as la_token and then la_token.is_function
		end

	is_main:BOOLEAN
			-- Is `Current' a complete abstract syntaxic tree of the main module
		do
			Result := attached token as la_token and then la_token.is_start
		end

	is_write:BOOLEAN
			-- Is `Current' representing a write (output) node
		do
			Result := attached token as la_token and then la_token.is_write
		end

	is_read:BOOLEAN
			-- Is `Current' representing a read (input) node
		do
			Result := attached token as la_token and then la_token.is_read
		end

	is_primitive:BOOLEAN
			-- Is `Current' representing a primitive value
		do
			if attached token as la_token then
				Result := 	la_token.is_string or la_token.is_integer or la_token.is_real or
							la_token.is_true or la_token.is_false or la_token.is_void
			else
				Result := False
			end
		end


	is_uniary_expression:BOOLEAN
			-- Is `Current' representing an expression that has one sub-expression value
		do
			if attached token as la_token then
				Result := la_token.is_not or la_token.is_lparan or
							(la_token.is_minus and children.count = 1)
			else
				Result := False
			end
		end


	is_binary_expression:BOOLEAN
			-- Is `Current' representing an expression that has two sub-expression values
		do
			if attached token as la_token then
				Result := 	la_token.is_plus or la_token.is_star or
							la_token.is_slash or la_token.is_div or
							la_token.is_mod or la_token.is_lshift or
							la_token.is_rshift or la_token.is_power or
							la_token.is_eq or la_token.is_lt or
							la_token.is_gt or la_token.is_le or
							la_token.is_ge or la_token.is_ne or
							la_token.is_xor or
							(la_token.is_minus and children.count = 2)
			else
				Result := False
			end
		end


	is_lazy_evaluated_binary_expression:BOOLEAN
			-- Is `Current' representing an expression that has two sub-expression values and is lazy evaluated
		do
			if attached token as la_token then
				Result := la_token.is_or or la_token.is_and or la_token.is_implies
			else
				Result := False
			end
		end

	is_type_checker:BOOLEAN
			-- Is `Current' a type validator
		do
			Result := attached token as la_token and then la_token.is_is
		end

	is_type_indicator:BOOLEAN
			-- Is `Current' a type indicator
		do
			Result := attached token as la_token and then (
								la_token.is_void or
								la_token.is_boolean_type or
								la_token.is_integer_type or
								la_token.is_real_type or
								la_token.is_list_type or
								la_token.is_file_type
							)
		end


invariant
	Is_Main_Valid: is_ready implies (is_main implies
								(attached token as la_token and then la_token.is_start) and
								children.count = 1 and children.has ("statements"))
	Is_Assignment_Valid: is_ready implies (is_assignment implies
									(attached token as la_token and then la_token.is_assignment) and
									children.count = 2 and children.has ("variable") and children.has ("expression"))
	Is_Procedure_Valid: is_ready implies (is_procedure implies
									(attached token as la_token and then la_token.is_procedure) and
									children.has ("name") and children.has ("statements"))
	Is_Function_Valid: is_ready implies (is_function implies
									(attached token as la_token and then la_token.is_function) and
									children.has ("name") and children.has ("statements") and children.has ("result"))
	Is_Variable_Valid: is_variable implies (attached token as la_token and then la_token.is_id)
	Is_List_Valid: (is_variable and is_list) implies
							((attached token as la_token and then la_token.is_id) and then
							(attached children.at ("index")))
	Is_Function_Call_Valid: is_function_call implies (attached token as la_token and then la_token.is_id)
	Is_Prcedure_Call_Valid: is_ready implies (is_procedure_call implies (attached token as la_token and then la_token.is_execute) and then
													(children.has ("name")))
	Is_Command_Call_Valid: is_ready implies (is_command_call implies (is_statement and attached token as la_token and then la_token.is_id))
	Is_If_And_ElseIf_Valid: is_ready implies ((is_if or is_elseif) implies
							(attached token as la_token and then (la_token.is_if or la_token.is_elseif) and
							attached children.at ("expression") and attached children.at ("statements")))
	Is_Else_Valid: is_ready implies (is_else implies
							(attached token as la_token and then la_token.is_else and
							attached children.at ("statements")))
	Is_while_and_until_Valid: is_ready implies ((is_while or is_until) implies
							(attached token as la_token and then (la_token.is_while or la_token.is_until) and
							attached children.at ("statements") and
							is_statement))
	Is_repeat_Valid: is_ready implies ((is_repeat) implies
							(attached token as la_token and then (la_token.is_repeat) and
							attached children.at ("statements") and attached children.at ("condition") and is_statement
							))
	Is_For_Valid: is_ready implies (is_for implies
										(attached token as la_token and then la_token.is_for and
										(attached children.at ("for_loop") as la_for_loop and then la_for_loop.is_for_loop) and
										is_statement
										)
									)
	Is_For_Loop_Valid: is_ready implies (is_for_loop implies
											(attached children.at ("variable_name") and
											attached children.at ("base_expression") and
											attached children.at ("limit_expression") and
											attached children.at ("statements") and
											is_statement
											)
										)
	Is_Type_Checker_Valid: is_ready implies (is_type_checker implies
												((attached children.at ("type") as la_type and then la_type.is_type_indicator) and
												(attached children.at ("expression") as la_expression and then la_expression.is_expression))
											)
	Result_Is_Exprssion: is_ready implies (is_result implies is_expression)

end
