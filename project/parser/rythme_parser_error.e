note
	description: "Summary description for {RYTHME_PARSER_ERROR}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	RYTHME_PARSER_ERROR

inherit
	RYTHME_ERROR
		redefine
			clear, exist
		end
	RYTHME_NODE_ERROR
		redefine
			clear, exist
		end

feature -- Access

	clear
			-- <Precursor>
		do
			Precursor {RYTHME_ERROR}
			module_name_not_valid := False
			module_name_already_in_use := False
			main_module_duplicated := False
		end

	exist:BOOLEAN
			-- <Precursor>
		do
			Result :=
					Precursor {RYTHME_ERROR} or
					module_name_not_valid or
					main_module_duplicated or
					module_name_already_in_use
		end

	module_name_not_valid:BOOLEAN
			-- `Current' represent an error in the naming of
			-- a module.

	set_module_name_not_valid(a_line, a_column:INTEGER)
			-- Set `Current' as an error in the naming of
			-- a module. The error occured at the line `a_line'
			-- and character `a_column' in the source input.
		do
			line := a_line
			column := a_column
			module_name_not_valid := True
		end

	set_module_name_not_valid_with_node(a_node:RYTHME_AST_NODE)
			-- Set `Current' as an error in the naming of
			-- a module. The error occured in `a_node'
		do
			set_line_and_column (a_node)
			module_name_not_valid := True
		end

	module_name_already_in_use:BOOLEAN
			-- `Current' represent an error in the naming of
			-- a module that is already in use.

	set_module_name_already_in_use(a_line, a_column:INTEGER; a_value:READABLE_STRING_GENERAL)
			-- Set `Current' as an error in the naming of
			-- a module that is already in use. The error occured at
			-- the line `a_line' and character `a_column' in the source
			-- input. The module name is represented by `a_value'
		do
			line := a_line
			column := a_column
			erroneous_value := a_value
			module_name_already_in_use := True
		end

	set_module_name_already_in_use_with_node(a_node:RYTHME_AST_NODE; a_value:READABLE_STRING_GENERAL)
			-- Set `Current' as an error in the naming of a module
			-- that is already in use. The error occured in `a_node'
		do
			set_line_and_column (a_node)
			erroneous_value := a_value
			module_name_already_in_use := True
		end

	main_module_duplicated:BOOLEAN
			-- `Current' represent a duplicating of the main
			-- module error

	set_main_module_duplicated(a_line, a_column:INTEGER)
			-- Set `Current' as a duplicating of the main
			-- module error. The error occured at
			-- the line `a_line' and character `a_column' in the source
			-- input. The module name is represented by `a_value'
		do
			line := a_line
			column := a_column
			main_module_duplicated := True
		end

	set_main_module_duplicated_with_node(a_node:RYTHME_AST_NODE)
			-- Set `Current' as a duplicating of the main
			-- module error. The error occured in `a_node'.
		do
			set_line_and_column (a_node)
			main_module_duplicated := True
		end

end
