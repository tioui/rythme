note

	description: "Parser token codes"
	generator: "geyacc version 3.9"

deferred class RYTHME_TOKENS

inherit

	YY_PARSER_TOKENS

feature -- Last values

	last_detachable_any_value: detachable ANY
	last_detachable_rythme_ast_node_value: detachable RYTHME_AST_NODE

feature -- Access

	token_name (a_token: INTEGER): STRING
			-- Name of token `a_token'
		do
			inspect a_token
			when 0 then
				Result := "EOF token"
			when -1 then
				Result := "Error token"
			when TR_COMMA then
				Result := "TR_COMMA"
			when TR_ASSIGNMENT then
				Result := "TR_ASSIGNMENT"
			when TR_EQ then
				Result := "TR_EQ"
			when TR_LT then
				Result := "TR_LT"
			when TR_GT then
				Result := "TR_GT"
			when TR_LE then
				Result := "TR_LE"
			when TR_GE then
				Result := "TR_GE"
			when TR_NE then
				Result := "TR_NE"
			when TR_LPARAN then
				Result := "TR_LPARAN"
			when TR_RPARAN then
				Result := "TR_RPARAN"
			when TR_LBRACE then
				Result := "TR_LBRACE"
			when TR_RBRACE then
				Result := "TR_RBRACE"
			when TR_LSQURE then
				Result := "TR_LSQURE"
			when TR_RSQURE then
				Result := "TR_RSQURE"
			when TR_PLUS then
				Result := "TR_PLUS"
			when TR_MINUS then
				Result := "TR_MINUS"
			when TR_STAR then
				Result := "TR_STAR"
			when TR_SLASH then
				Result := "TR_SLASH"
			when TR_POWER then
				Result := "TR_POWER"
			when TR_LSHIFT then
				Result := "TR_LSHIFT"
			when TR_RSHIFT then
				Result := "TR_RSHIFT"
			when TR_DIV then
				Result := "TR_DIV"
			when TR_MOD then
				Result := "TR_MOD"
			when TR_FREE then
				Result := "TR_FREE"
			when TR_ID then
				Result := "TR_ID"
			when TR_INTEGER then
				Result := "TR_INTEGER"
			when TR_REAL then
				Result := "TR_REAL"
			when TR_STRING then
				Result := "TR_STRING"
			when TR_IF then
				Result := "TR_IF"
			when TR_THEN then
				Result := "TR_THEN"
			when TR_ELSE then
				Result := "TR_ELSE"
			when TR_ELSEIF then
				Result := "TR_ELSEIF"
			when TR_ENDIF then
				Result := "TR_ENDIF"
			when TR_FOR then
				Result := "TR_FOR"
			when TR_FROM then
				Result := "TR_FROM"
			when TR_TO then
				Result := "TR_TO"
			when TR_DO then
				Result := "TR_DO"
			when TR_ENDFOR then
				Result := "TR_ENDFOR"
			when TR_WHILE then
				Result := "TR_WHILE"
			when TR_ENDWHILE then
				Result := "TR_ENDWHILE"
			when TR_UNTIL then
				Result := "TR_UNTIL"
			when TR_ENDUNTIL then
				Result := "TR_ENDUNTIL"
			when TR_OR then
				Result := "TR_OR"
			when TR_AND then
				Result := "TR_AND"
			when TR_NOT then
				Result := "TR_NOT"
			when TR_IS then
				Result := "TR_IS"
			when TR_VOID then
				Result := "TR_VOID"
			when TR_BOOLEAN_TYPE then
				Result := "TR_BOOLEAN_TYPE"
			when TR_INTEGER_TYPE then
				Result := "TR_INTEGER_TYPE"
			when TR_REAL_TYPE then
				Result := "TR_REAL_TYPE"
			when TR_LIST_TYPE then
				Result := "TR_LIST_TYPE"
			when TR_FILE_TYPE then
				Result := "TR_FILE_TYPE"
			when TR_EXECUTE then
				Result := "TR_EXECUTE"
			when TR_FALSE then
				Result := "TR_FALSE"
			when TR_TRUE then
				Result := "TR_TRUE"
			when TR_PROCEDURE then
				Result := "TR_PROCEDURE"
			when TR_FUNCTION then
				Result := "TR_FUNCTION"
			when TR_RETURN then
				Result := "TR_RETURN"
			when TR_XOR then
				Result := "TR_XOR"
			when TR_IMPLIES then
				Result := "TR_IMPLIES"
			when TR_WRITE then
				Result := "TR_WRITE"
			when TR_READ then
				Result := "TR_READ"
			when TR_IN then
				Result := "TR_IN"
			when TR_START then
				Result := "TR_START"
			when TR_END then
				Result := "TR_END"
			when TR_NEWLINE then
				Result := "TR_NEWLINE"
			when TR_STEP then
				Result := "TR_STEP"
			when TR_REPEAT then
				Result := "TR_REPEAT"
			when TR_ENDREPEAT then
				Result := "TR_ENDREPEAT"
			else
				Result := yy_character_token_name (a_token)
			end
		end

feature -- Token codes

	TR_COMMA: INTEGER = 258
	TR_ASSIGNMENT: INTEGER = 259
	TR_EQ: INTEGER = 260
	TR_LT: INTEGER = 261
	TR_GT: INTEGER = 262
	TR_LE: INTEGER = 263
	TR_GE: INTEGER = 264
	TR_NE: INTEGER = 265
	TR_LPARAN: INTEGER = 266
	TR_RPARAN: INTEGER = 267
	TR_LBRACE: INTEGER = 268
	TR_RBRACE: INTEGER = 269
	TR_LSQURE: INTEGER = 270
	TR_RSQURE: INTEGER = 271
	TR_PLUS: INTEGER = 272
	TR_MINUS: INTEGER = 273
	TR_STAR: INTEGER = 274
	TR_SLASH: INTEGER = 275
	TR_POWER: INTEGER = 276
	TR_LSHIFT: INTEGER = 277
	TR_RSHIFT: INTEGER = 278
	TR_DIV: INTEGER = 279
	TR_MOD: INTEGER = 280
	TR_FREE: INTEGER = 281
	TR_ID: INTEGER = 282
	TR_INTEGER: INTEGER = 283
	TR_REAL: INTEGER = 284
	TR_STRING: INTEGER = 285
	TR_IF: INTEGER = 286
	TR_THEN: INTEGER = 287
	TR_ELSE: INTEGER = 288
	TR_ELSEIF: INTEGER = 289
	TR_ENDIF: INTEGER = 290
	TR_FOR: INTEGER = 291
	TR_FROM: INTEGER = 292
	TR_TO: INTEGER = 293
	TR_DO: INTEGER = 294
	TR_ENDFOR: INTEGER = 295
	TR_WHILE: INTEGER = 296
	TR_ENDWHILE: INTEGER = 297
	TR_UNTIL: INTEGER = 298
	TR_ENDUNTIL: INTEGER = 299
	TR_OR: INTEGER = 300
	TR_AND: INTEGER = 301
	TR_NOT: INTEGER = 302
	TR_IS: INTEGER = 303
	TR_VOID: INTEGER = 304
	TR_BOOLEAN_TYPE: INTEGER = 305
	TR_INTEGER_TYPE: INTEGER = 306
	TR_REAL_TYPE: INTEGER = 307
	TR_LIST_TYPE: INTEGER = 308
	TR_FILE_TYPE: INTEGER = 309
	TR_EXECUTE: INTEGER = 310
	TR_FALSE: INTEGER = 311
	TR_TRUE: INTEGER = 312
	TR_PROCEDURE: INTEGER = 313
	TR_FUNCTION: INTEGER = 314
	TR_RETURN: INTEGER = 315
	TR_XOR: INTEGER = 316
	TR_IMPLIES: INTEGER = 317
	TR_WRITE: INTEGER = 318
	TR_READ: INTEGER = 319
	TR_IN: INTEGER = 320
	TR_START: INTEGER = 321
	TR_END: INTEGER = 322
	TR_NEWLINE: INTEGER = 323
	TR_STEP: INTEGER = 324
	TR_REPEAT: INTEGER = 325
	TR_ENDREPEAT: INTEGER = 326

end
