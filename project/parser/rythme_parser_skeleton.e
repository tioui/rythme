note
	description: "Skeleton (Base method and attributes) for the Rythme parser."
	author: "Louis Marchand"
	date: "2014, October 15"
	revision: "0.3"

deferred class
	RYTHME_PARSER_SKELETON

inherit
	YY_PARSER_SKELETON
		rename
			make as make_parser
		redefine
			default_create,
			make_parser,
			report_error
		end

feature {NONE} -- Initialization

	make_parser
			-- Initialization of `Current' without scanner initialization
		do
			Precursor {YY_PARSER_SKELETON}
			create i_ast
			create parser_error
			set_node_as_ready(i_ast)
		end

	default_create
			-- Initialization of `Current' including scanner initialization
			-- using the standard console input as source input
		do
			default_create_scanner
			make_parser
		end

	make_with_file_name(a_file_name:READABLE_STRING_GENERAL)
			-- Initialization of `Current' including scanner initialization
			-- using `a_file_name' to open the source input file
		do
			make_with_file_name_scanner(a_file_name)
			make_parser
		end

	make_with_string (a_string:READABLE_STRING_GENERAL)
			-- Initialization of `Current' including scanner initialization
			-- using `a_string' as source input
		do
			make_with_file_name_scanner(a_string)
			make_parser
		end

	default_create_scanner
			-- The default constructor of the scanner
		deferred
		end

	make_with_file_name_scanner (a_file_name:READABLE_STRING_GENERAL)
			-- The constructor of the scanner that use `a_file_name' to
			-- open the source input file.
		deferred
		end

	make_with_string_scanner (a_string:READABLE_STRING_GENERAL)
			-- The constructor of the scanner that use `a_string' as
			-- source input.
		deferred
		end

feature -- Access

	source:READABLE_STRING_GENERAL
			-- The source code (file name or index) that `Current' is parsing
		deferred
		end

	set_ast(a_ast:RYTHME_AST_NODE)
			-- Tell `Current' to not create a new abstract syntax tree but to append a
			-- new node to the root of `a_ast'
		require
			Ast_Is_Valid: node_is_valid(a_ast)
		do
			i_ast := a_ast
		end

	ast:RYTHME_AST_NODE assign set_ast
			-- The resulting ast abstract syntax tree generate from
			-- the source input if the parsing `is_accepted'
		require
			Parsing_Succeeded: is_accepted
		do
			Result := i_ast
		ensure
			Ast_Is_Valid: node_is_valid(i_ast)
		end

	is_accepted:BOOLEAN
			-- The parsing has terminate successfully. The resuling
			-- abstract syntax tree can be retreive from `i_ast'
		do
			Result := yy_parsing_status = yyAccepted
		end

	parser_error:RYTHME_PARSER_ERROR
			-- Error manager

	has_parser_error:BOOLEAN
			-- Is `Current' in a parsing error state
		do
			Result := parser_error.exist
		end

	line:INTEGER
			-- The line in the input source that is currently processed
		deferred
		end

	column:INTEGER
			-- The position in the line of the input source that is currently processed
		deferred
		end

	node_is_valid(a_node:detachable RYTHME_AST_NODE):BOOLEAN
			-- Check if every node of the sub-tree having `a_node' as root node is ready to be interpreted
		note
			todo: "The method!!!"
		do
			if attached a_node then
				Result := a_node.is_ready
				Result := Result and across a_node.children as la_children all node_is_valid(la_children.item) end
			else
				Result := True
			end
		end

feature {NONE} -- Implementation

	i_ast:RYTHME_AST_NODE
			-- The resulting ast abstract syntax tree generate from
			-- the source input if the parsing `is_accepted'

	set_node_as_ready(a_node:detachable RYTHME_AST_NODE)
			-- Indicate that the node is ready to be interpreted
		require
			Node_Valid: attached a_node
		do
			if attached a_node then
				a_node.set_is_ready
			else
				parser_error.set_internal_error_with_position (line, column)
				parser_error.set_source (source)
				abort
			end
		end

	set_node_as_main(a_main, a_statements:detachable RYTHME_AST_NODE)
			-- Add `a_statements' as a children of `a_main' node
			-- Warning: Side effect on `a_main'
		require
			Main_Node_Valid: attached a_main
			Statements_Valid: attached a_statements as la_statements implies la_statements.is_statements
		do
			if attached a_main then
				set_node_as_statements_contener(a_main, a_statements)
				set_node_as_ready(a_main)
				if i_ast.children.has ("main") then
					parser_error.set_main_module_duplicated_with_node (a_main)
					parser_error.set_source (source)
					abort
				else
					i_ast.children.extend (a_main, "main")
				end
			else
				parser_error.set_internal_error_with_position (line, column)
				parser_error.set_source (source)
				abort
			end
		ensure
			Main_Node_Valid: attached a_main as la_main implies la_main.is_main
		end

	set_node_as_module(a_node, a_name, a_arguments, a_statements, a_return:detachable RYTHME_AST_NODE)
			-- Add `a_statements' as a children of `a_node'. `a_node' is a module node
			-- having `a_name' and returning the value of `a_return' expression node.
			-- Warning: Side effect on `a_node'
		require
			Node_Valid: attached a_node
			Name_Valid: attached a_name
			Statements_Valid: attached a_statements as la_statements implies la_statements.is_statements
			Return_Valid: attached a_return as la_return implies la_return.is_expression
		do
			if attached a_node then
				a_node.children.extend (a_name, "name")
				a_node.children.extend (a_arguments, "arguments")
				if attached a_return then
					a_return.set_is_statement
					a_return.set_is_result
					a_node.children.extend (a_return, "result")
				end
				set_node_as_statements_contener(a_node, a_statements)
				set_node_as_ready(a_node)
				if attached a_name and then
						attached a_name.token as la_string_token and then
						la_string_token.is_id and then not la_string_token.text.is_empty then
					set_node_as_ready (a_name)
					if i_ast.children.has (la_string_token.text) then
						parser_error.set_module_name_already_in_use_with_node (a_name, la_string_token.text)
						parser_error.set_source (source)
						abort
					else
						i_ast.children.extend (a_node, la_string_token.text)
					end
				else
					parser_error.set_module_name_not_valid_with_node (a_node)
					parser_error.set_source (source)
					abort
				end

			else
				parser_error.set_internal_error_with_position (line, column)
				parser_error.set_source (source)
				abort
			end
		ensure
			Node_Valid: attached a_node as la_node implies (la_node.is_function or la_node.is_procedure)
		end


	create_statements_node(a_statement, a_statements:detachable RYTHME_AST_NODE):RYTHME_AST_NODE
			-- Create a statements node containing a direct `a_statement' and a tree of others `a_statements'
		require
			Statement_Valid: attached a_statement as la_statement and then la_statement.is_statement
			Statements_Valid: attached a_statements as la_statements implies la_statements.is_statements
		do
			if attached a_statement then
				create Result.make_statements (a_statement, a_statements)
			else
				create Result
				parser_error.set_internal_error_with_position (line, column)
				parser_error.set_source (source)
				abort
			end
		end

	add_to_assignment_node(a_node, a_id, a_expression:detachable RYTHME_AST_NODE)
			-- Add the variable identifier contain in `a_id'and the value `a_expression' node
			-- in the assignement `a_node'
			-- Warning: Side effect on `a_node'
		require
			Node_Valid: attached a_node
			Id_Valid: attached a_id
			Expression_Valid:
						attached a_expression as la_expression and then
						(la_expression.is_expression or la_expression.is_read)
		do
			if attached a_node then
				a_node.children.extend (a_id, "variable")
				a_node.children.extend (a_expression, "expression")
			else
				parser_error.set_internal_error_with_position (line, column)
				parser_error.set_source (source)
				abort
			end
		ensure
			Node_Valid: attached a_node as la_node implies la_node.is_assignment
		end

	set_node_as_arguments(a_node, a_name, a_arguments:detachable RYTHME_AST_NODE)
			-- Set `a_node' as an argument node having the name `a_name' and chaining to optional other `a_arguments' node
		require
			Node_Valid: attached a_node
		do
			if attached a_node then
				a_node.children.extend (a_name, "name")
				a_node.children.extend (a_arguments, "arguments")
				set_node_as_ready(a_node)
			else
				parser_error.set_internal_error_with_position (line, column)
				parser_error.set_source (source)
				abort
			end
		end

	set_node_as_statement(a_node:detachable RYTHME_AST_NODE)
			-- Set `a_node' as a statement node.
			-- Warning: Side effect on `a_node'
		require
			Node_Valid: attached a_node
		do
			if attached a_node then
				a_node.set_is_statement
				set_node_as_ready(a_node)
			else
				parser_error.set_internal_error_with_position (line, column)
				parser_error.set_source (source)
				abort
			end
		end

	set_node_as_expression(a_node:detachable RYTHME_AST_NODE)
			-- Set `a_node' as an expression node
			-- Warning: Side effect on `a_node'
		require
			Node_Valid: attached a_node
		do
			if attached a_node then
				a_node.set_is_expression
				set_node_as_ready(a_node)
			else
				parser_error.set_internal_error_with_position (line, column)
				parser_error.set_source (source)
				abort
			end
		end

	set_node_as_execute(a_node, a_name, a_arguments:detachable RYTHME_AST_NODE)
			-- Set `a_node' as a procedure execution. The procedure name is in `a_name'
			-- and the arguments list is in `a_arguments'
			-- Warning: Side effect on `a_node'
		require
			Node_Valid: attached a_node
		do
			if attached a_node then
				set_node_as_ready (a_name)
				a_node.children.extend (a_name, "name")
				a_node.children.extend (a_arguments, "arguments")
				set_node_as_ready(a_node)
			else
				parser_error.set_internal_error_with_position (line, column)
				parser_error.set_source (source)
				abort
			end
		end

	add_expressions_to_node(a_expression, a_expressions:detachable RYTHME_AST_NODE)
			-- Add `a_expressions' tree as an `a_expression' children
			-- Warning: Side effect on `a_expression'
		require
			Expression_Valid: attached a_expression
		do
			if attached a_expression then
				a_expression.children.extend (a_expressions, "expressions")
			else
				parser_error.set_internal_error_with_position (line, column)
				parser_error.set_source (source)
				abort
			end
		end

	set_node_as_variable(a_node:detachable RYTHME_AST_NODE)
			-- Set `a_node' as a node containing a variable ID
			-- Warning: Side effect on `a_node'
		require
			Node_Valid: attached a_node
		do
			if attached a_node then
				a_node.set_is_variable
			else
				parser_error.set_internal_error_with_position (line, column)
				parser_error.set_source (source)
				abort
			end
		ensure
			Node_Is_Variable: attached a_node as la_node and then la_node.is_variable
		end

	set_node_as_list(a_node, a_index:detachable RYTHME_AST_NODE)
			-- Set `a_node' as a node containing a variable ID and list index
			-- Warning: Side effect on `a_node'
		require
			Node_Valid: attached a_node
		do
			if attached a_node then
				a_node.children.put (a_index, "index")
				a_node.set_is_list
				a_node.set_is_variable
			else
				parser_error.set_internal_error_with_position (line, column)
				parser_error.set_source (source)
				abort
			end
		end

	add_expression_to_node_with_index(a_node, a_expression:detachable RYTHME_AST_NODE; index:INTEGER)
			-- Add `a_expression' as a children to `a_mode' at a certain `index'
			-- Warning: Side effect on `a_node'
		require
			Node_Valid: attached a_node
		do
			if attached a_node then
				a_node.children.extend (a_expression, "expression_" + index.out)
			else
				parser_error.set_internal_error_with_position (line, column)
				parser_error.set_source (source)
				abort
			end
		end

	add_expression_to_node(a_node, a_expression:detachable RYTHME_AST_NODE)
			-- Add `a_expression' as a children to `a_mode'
			-- Warning: Side effect on `a_node'
		require
			Node_Valid: attached a_node
		do
			if attached a_node then
				a_node.children.extend (a_expression, "expression")
			else
				parser_error.set_internal_error_with_position (line, column)
				parser_error.set_source (source)
				abort
			end
		end

	add_type_to_node(a_node, a_type:detachable RYTHME_AST_NODE)
			-- Add `a_type' indicator to a casting `a_node' (TR_IS token)
			-- Warning: Side effect on `a_node'
		require
			Node_Valid: attached a_node
		do
			if attached a_node then
				a_node.children.extend (a_type, "type")
			else
				parser_error.set_internal_error_with_position (line, column)
				parser_error.set_source (source)
				abort
			end
		end

	add_variable_to_node(a_node, a_variable:detachable RYTHME_AST_NODE)
			-- Add `a_variable' ID to `a_node'
			-- Warning: Side effect on `a_node'
		require
			Node_Valid: attached a_node
		do
			if attached a_node then
				a_node.children.extend (a_variable, "variable")
			else
				parser_error.set_internal_error_with_position (line, column)
				parser_error.set_source (source)
				abort
			end
		end

	set_node_as_function_without_argument(a_node:detachable RYTHME_AST_NODE)
			-- Set `a_node' as a node containing a function ID.
			-- Warning: Side effect on `a_node'
		require
			Node_Valid: attached a_node
		do
			if attached a_node then
				a_node.set_is_function_call
				set_node_as_expression(a_node)
			else
				parser_error.set_internal_error_with_position (line, column)
				parser_error.set_source (source)
				abort
			end
		end

	set_node_as_function_with_arguments(a_node, a_expression, a_expressions:detachable RYTHME_AST_NODE)
			-- Set `a_node' as a node containing a function ID.
			-- The fonction has at least one argument from `a_expression' and optionnaly more in `a_expressions'
			-- Warning: Side effect on `a_node'
		require
			Node_Valid: attached a_node
		do
			if attached a_node then
				add_expression_to_node(a_node, a_expression)
				a_node.children.extend (a_expressions, "expressions")
				set_node_as_function_without_argument(a_node)
			else
				parser_error.set_internal_error_with_position (line, column)
				parser_error.set_source (source)
				abort
			end
		end

	set_node_as_inline_list(a_node, a_expression, a_expressions:detachable RYTHME_AST_NODE)
			-- Set `a_node' as a node containing an inline list.
			-- The fonction has optionnaly one value in `a_expression' and possibly more value in `a_expressions'
			-- Warning: Side effect on `a_node'
		require
			Node_Valid: attached a_node
		do
			if attached a_node then
				a_node.set_is_list
				a_node.children.extend (a_expression, "expression")
				a_node.children.extend (a_expressions, "expressions")
				set_node_as_expression (a_node)
			else
				parser_error.set_internal_error_with_position (line, column)
				parser_error.set_source (source)
				abort
			end
		end

	set_node_as_command(a_node, a_expression, a_expressions:detachable RYTHME_AST_NODE)
			-- Set `a_node' as a node containing a System command call using `a_expression' and `a_expressions'
			-- as procedure arguments
			-- Warning: Side effect on `a_node'
		require
			Node_Valid: attached a_node
		do
			if attached a_node then
				add_expression_to_node (a_node, a_expression)
				add_expressions_to_node (a_node, a_expressions)
				a_node.set_is_command_call
			else
				parser_error.set_internal_error_with_position (line, column)
				parser_error.set_source (source)
				abort
			end
		end

	set_node_as_if(a_node, a_expression, a_statements, a_else:detachable RYTHME_AST_NODE)
			-- Set `a_node' as a node containing a "If" control structure with `a_expression' as condition,
			-- `a_statements' as true execution statements, `a_elseif' is optionnaly one or more elseif clause
			-- and `a_else' is an optionnal else clause.
			-- Warning: Side effect on `a_node'
		require
			Node_Valid: attached a_node
		do
			if attached a_node then
				a_node.children.extend (a_else, "else")
				set_node_as_conditionnal_statements(a_node, a_expression, a_statements)
			else
				parser_error.set_internal_error_with_position (line, column)
				parser_error.set_source (source)
				abort
			end
		end

	set_node_as_statements_contener(a_node, a_statements:detachable RYTHME_AST_NODE)
			-- Set `a_node' as a node containing a control structure with `a_statements' as to execute.
			-- Warning: Side effect on `a_node'
		require
			Node_Valid: attached a_node
		do
			if attached a_node then
				a_node.children.extend (a_statements, "statements")
			else
				parser_error.set_internal_error_with_position (line, column)
				parser_error.set_source (source)
				abort
			end
		end

	set_node_as_else(a_node, a_statements:detachable RYTHME_AST_NODE)
			-- Set `a_node' as a node containing a control structure with `a_statements' as to execute.
			-- Warning: Side effect on `a_node'
		require
			Node_Valid: attached a_node
		do
			if attached a_node then
				set_node_as_statements_contener(a_node, a_statements)
				set_node_as_ready(a_node)
			else
				parser_error.set_internal_error_with_position (line, column)
				parser_error.set_source (source)
				abort
			end
		end

	set_node_as_conditionnal_statements(a_node, a_expression, a_statements:detachable RYTHME_AST_NODE)
			-- Set `a_node' as a node containing a control structure containing a conditionnal expression and
			-- `a_statements' to execute depending on the conditionnal expression
			-- Warning: Side effect on `a_node'
		require
			Node_Valid: attached a_node
		do
			if attached a_node then
				a_node.children.extend (a_expression, "expression")
				set_node_as_statements_contener(a_node, a_statements)
			else
				parser_error.set_internal_error_with_position (line, column)
				parser_error.set_source (source)
				abort
			end
		end

	set_node_as_repeat(a_node, a_statements, a_condition:detachable RYTHME_AST_NODE)
			-- Set `a_node' as a node containing a repeat statement using a while or until
			-- statement in `a_condition' and using `a_statements' as loop body
			-- Warning: Side effect on `a_node'
		require
			Node_Valid: attached a_node
		do
			if attached a_node then
				a_node.children.extend (a_condition, "condition")
				set_node_as_statements_contener(a_node, a_statements)
			else
				parser_error.set_internal_error_with_position (line, column)
				parser_error.set_source (source)
				abort
			end
		end

	set_node_as_for(a_node, a_variable_name, a_base_expression, a_limit_expression, a_step, a_statements:detachable RYTHME_AST_NODE)
			-- Set `a_node' as a node containing a for control structure. The increment variable will be `a_variable'
			-- The incremental variable identified by `a_variable_name' will start at the value of `a_base_expression'
			-- and will stop when the value of `a_limit_expression' will be reach. At each incrementation, `a_statements'
			-- will be execute. `a_step' indicate the optionnal expression containing the value to increment at each incrementation.
			-- Warning: Side effect on `a_node'
		require
			Node_Valid: attached a_node
		do
			if
				attached a_node and attached a_variable_name and attached a_limit_expression and
				attached a_base_expression and attached a_statements
			then
				a_node.children.extend (
						create {RYTHME_AST_NODE}.make_for_loop (
								a_variable_name, a_base_expression, a_limit_expression, a_statements, a_step),
						"for_loop"
					)
			else
				parser_error.set_internal_error_with_position (line, column)
				parser_error.set_source (source)
				abort
			end
		end

	report_error(a_message:STRING_8)
			-- <Precursor>
		do
			parser_error.set_syntax_not_valid (line, column)
			parser_error.set_source (source)
			abort
		end

end
