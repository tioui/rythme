note
	description: "Interpreter of a Rythme abstract syntax tree."
	author: "Louis Marchand"
	date: "2014, November 5"
	revision: "0.1"

class
	RYTHME_INTERPRETER

inherit
	VALUE_TOOLS
		redefine
			error
		end

create
	make,
	make_with_constants,
	make_and_run

feature {NONE} -- Initialization

	make(a_ast:RYTHME_AST_NODE; a_modules:STRING_TABLE[RYTHME_AST_NODE])
			-- Initialize the interpreter using `a_ast' as syntax tree
		require
			Ast_Is_Module: a_ast.is_main or a_ast.is_function or a_ast.is_procedure
		do
			make_with_constants(a_ast, a_modules, create {CONSTANTS})
		end

	make_with_constants(a_ast:RYTHME_AST_NODE; a_modules:STRING_TABLE[RYTHME_AST_NODE]; a_constants:CONSTANTS)
			-- Initialize the interpreter using `a_ast' as syntax tree and using the locals in `a_constants' for text output
		require
			Ast_Is_Module: a_ast.is_main or a_ast.is_function or a_ast.is_procedure
		do
			create {LINKED_STACK[detachable RYTHME_AST_NODE]} to_execute.make
			system_modules := a_modules
			set_module_name(a_ast)
			if a_ast.is_function then
				to_execute.extend (a_ast.children.at("result"))
			end
			to_execute.extend (a_ast.children.at("statements"))
			set_true_text (a_constants.true_text)
			set_false_text (a_constants.false_text)
			set_void_text(a_constants.void_text)
			set_file_text(a_constants.file_text)
			set_print_agent(agent default_print)
			set_read_agent(agent default_read)
			create variables.make_caseless (0)
			create functions.make_caseless (0)
			create commands.make_caseless (0)
			create {LINKED_LIST[TUPLE[RYTHME_AST_NODE, detachable ANY]]}called_functions_returned_value.make
			create returned_value_action
			create error
		end

	make_and_run(a_ast:RYTHME_AST_NODE; a_modules:STRING_TABLE[RYTHME_AST_NODE])
			-- Initialize the interpreter using `a_ast' as syntax tree and
			-- run the interpreter
		require
			Ast_Is_Module: a_ast.is_main or a_ast.is_function or a_ast.is_procedure
		do
			make(a_ast, a_modules)
			run
		end


feature -- Access

	run
			-- Run the interpreter
		do
			from
			until
				is_success or has_error
			loop
				execute_one_statement
				if not has_error and is_module_requested then
					execute_module
				end
			end
		end

	requested_module_interpreter:detachable RYTHME_INTERPRETER
			-- When `is_module_requested' is set, the interpreter to execute the module
		do
			if attached requested_module as la_module then
				create Result.make (la_module.ast, system_modules)
				Result.set_true_text (true_text)
				Result.set_false_text (false_text)
				Result.set_void_text(void_text)
				Result.set_file_text(file_text)
				Result.set_print_agent(print_agent)
				Result.set_read_agent(read_agent)
				Result.add_variables(requested_module_arguments)
				Result.set_functions (functions)
				Result.set_commands (commands)
				Result.returned_value_action.extend (agent set_called_function_returned_value)
			end
		end

	execute_module
			-- Execute a requested module
		do
			if attached requested_module_interpreter as la_interpreter then
				la_interpreter.run
			end
		end

	requested_module_arguments:STRING_TABLE[detachable ANY]
			-- Get the module call arguments to asign to module variables
		do
			if
				attached requested_module as la_module and then
				attached la_module.ast.children.at("arguments") as la_arguments
			then
				Result := generate_module_argument(la_arguments, la_module.arguments, 1)
			else
				create Result.make (0)
			end
		end

	execute_one_statement
			-- Execute one statement (a code line)
		do
			error.clear
			is_module_requested := False
			if not exhausted then
				if attached to_execute.item as la_node then
					if la_node.is_statements then
						extract_top_statements
						execute_one_statement
					elseif la_node.is_statement then
						execute_top_statement
					end
				else
					to_execute.remove
					execute_one_statement
				end
			end
		end

	system_modules:STRING_TABLE[RYTHME_AST_NODE] assign set_system_modules
			-- Every module declared in the system

	set_system_modules(a_modules:STRING_TABLE[RYTHME_AST_NODE])
			-- Assign the module table `a_modules' to `system_modules'
		do
			system_modules := a_modules
		end

	module_name:READABLE_STRING_GENERAL
			-- The name of the module that `Current' is interpreting

	is_success:BOOLEAN
			-- `Current' successfully run the module
		do
			Result := exhausted and not has_error
		end

	has_error:BOOLEAN
			-- An error occured while executing the last statement
		do
			Result := error.exist
		end

	error:INTERPRETER_ERROR
			-- Represent the information on the error when an error
			-- occured in `Current' (when `has_error' is True)

	return_value:detachable ANY
			-- If `Current' interpret a function, when `is_success' is True,
			-- contain the value returned by the function

	print_agent:PROCEDURE[ANY, TUPLE[READABLE_STRING_GENERAL]]
			-- To launch when `Current' need to print on the screen.

	set_print_agent(a_print_agent:PROCEDURE[ANY, TUPLE[READABLE_STRING_GENERAL]])
			-- Assign `a_print_agent' to `print_agent'
		do
			print_agent := a_print_agent
		end

	read_agent:FUNCTION[ANY, TUPLE,READABLE_STRING_GENERAL]
			-- To launch when `Current' need to read on the screen.

	set_read_agent(a_read_agent:FUNCTION[ANY, TUPLE,READABLE_STRING_GENERAL])
			-- Assign `a_read_agent' to `read_agent'
		do
			read_agent := a_read_agent
		end

	variables:STRING_TABLE[detachable ANY]
			-- Map containing every variable name (`key') and there values

	add_variables(a_variables:STRING_TABLE[detachable ANY])
			-- Add `a_variables' values and name to `variables'
			-- Can be use when adding argument to moule call
		do
			variables.merge (a_variables)
		end

	functions:STRING_TABLE[FUNCTION[ANY, TUPLE[error:INTERPRETER_ERROR;arguments:CHAIN[detachable ANY]], detachable ANY]] assign set_functions
			-- Map containing every function name (`key') and the agent
			-- to launch the function (`value')

	set_functions(a_functions:STRING_TABLE[FUNCTION[ANY, TUPLE[error:INTERPRETER_ERROR;arguments:CHAIN[detachable ANY]], detachable ANY]])
			-- Assign `a_functions' to `functions'
		do
			functions := a_functions
		end

	commands:STRING_TABLE[PROCEDURE[ANY, TUPLE[error:INTERPRETER_ERROR;arguments:CHAIN[detachable ANY]]]] assign set_commands
			-- Map containing every command name (`key') and the agent
			-- to launch the command (`value')

	set_commands(a_commands:STRING_TABLE[PROCEDURE[ANY, TUPLE[error:INTERPRETER_ERROR;arguments:CHAIN[detachable ANY]]]])
			-- Assign `a_commands' to `commands'
		do
			commands := a_commands
		end

	add_functions(a_functions:STRING_TABLE[FUNCTION[ANY, TUPLE[error:INTERPRETER_ERROR;arguments:CHAIN[detachable ANY]], detachable ANY]])
			-- Add all `function_agent' of the table `a_functions' in `functions'
		do
			functions.merge (a_functions)
		end

	add_commands(a_commands:STRING_TABLE[PROCEDURE[ANY, TUPLE[error:INTERPRETER_ERROR;arguments:CHAIN[detachable ANY]]]])
			-- Add all `function_agent' of the table `a_functions' in `functions'
		do
			commands.merge (a_commands)
		end

	to_execute:STACK[detachable RYTHME_AST_NODE]
			-- The execution {STACK}. The `top' of the {STACK} is the next
			-- statement to be executed.

	exhausted:BOOLEAN
			-- Is the execution {STACK} `to_execute' empty
		do
			Result := to_execute.is_empty
		end

	is_module_requested:BOOLEAN
			-- Is the current statements requested a module function execution

	requested_module:detachable TUPLE[is_function:BOOLEAN;ast:RYTHME_AST_NODE; arguments:CHAIN[detachable ANY]]
			-- if `is_module_requested' is set, module that has been requested by `last_module_call'

	last_module_call:detachable RYTHME_AST_NODE
			-- When `is_module_requested' is set, the {RYTHME_AST_NODE} containing the module call

	called_functions_returned_value:LIST[TUPLE[node:RYTHME_AST_NODE; value:detachable ANY]]
			-- The returned value of the last function module called

	set_called_function_returned_value(a_value:detachable ANY)
			-- Assign last called function module returned value `a_value' to `called_functions_returned_value'
		require
			Is_Function_Module_Called: attached requested_module as la_requested_module and then la_requested_module.is_function
		do
			if attached last_module_call as la_last_module_call then
				called_functions_returned_value.extend ([la_last_module_call, a_value])
			end
		end

	returned_value_action:ACTION_SEQUENCE[TUPLE[value:detachable ANY]]
			-- An agent to execute when `Current' is a function and has � value to return to the caller.


feature {NONE} -- Implementation

	extract_top_statements
			-- Extract the statements (list of statement) on the top of the
			-- execution {STACK} `to_execute' and put every containing statement
			-- on the {STACK} `to_execute'.
		require
			attached to_execute.item as la_node and then la_node.is_statements
		local
			l_statement:RYTHME_AST_NODE
			l_statements:detachable RYTHME_AST_NODE
		do
			if
				attached to_execute.item as la_node and then
				attached la_node.children.at("statement") as la_statement
			then
				l_statement := la_statement
				la_node.children.search ("statements")
				if la_node.children.found then
					l_statements := la_node.children.found_item
					to_execute.remove
					to_execute.extend (l_statements)
					to_execute.extend (l_statement)
				end
			end
		end

	execute_top_statement
			-- Execute the statement at the top of the `to_execute' {STACK}
			-- Warning, can set the `is_module_requested'
		require
			Top_Statement_Valid: attached to_execute.item as la_node and then la_node.is_statement
		do
			if attached to_execute.item as la_node then
				to_execute.remove
				if la_node.is_write then
					execute_write(la_node)
				elseif la_node.is_assignment then
					execute_assignment(la_node)
				elseif la_node.is_if then
					manage_if_statement(la_node)
				elseif la_node.is_while then
					manage_while_and_until_statement(la_node)
				elseif la_node.is_until then
					manage_while_and_until_statement(la_node)
				elseif la_node.is_for then
					manage_for_statement(la_node)
				elseif la_node.is_for_loop then
					manage_for_loop_statement(la_node)
				elseif la_node.is_procedure_call then
					manage_procedure_call(la_node)
				elseif la_node.is_result then
					manage_function_result(la_node)
				elseif la_node.is_command_call then
					manage_command_call(la_node)
				elseif la_node.is_repeat then
					manage_repeat_statement (la_node)
				else
					error.set_internal_error_with_node (la_node)
				end
				if
					(not has_error and is_module_requested) and then
					attached requested_module as la_module and then la_module.is_function
				then
					to_execute.extend (la_node)
				else
					called_functions_returned_value.wipe_out
				end
			end
		end

		manage_command_call(a_call:RYTHME_AST_NODE)
			-- Prepare the system command call `a_call'.
			-- Warning, can set the `is_module_requested'
		require
			Call_Is_Procedure_Call: a_call.is_command_call
		local
			l_arguments:CHAIN[detachable ANY]
		do
			l_arguments := expressions_to_chain (a_call)
			if not has_error and not is_module_requested then
				if attached a_call.token as la_token then
					if attached commands.at (la_token.text) as la_command then
						la_command.call ([error, l_arguments])
						if has_error then
							error.set_line_and_column (a_call)
						end
					else
						error.set_command_name_not_valid (a_call, la_token.text)
					end
				else
					error.set_internal_error_with_node (a_call)
				end
			end

		end

	manage_procedure_call(a_call:RYTHME_AST_NODE)
			-- Prepare the procedure call `a_call'. It the procedure call is valid, set the `is_module_requested' flag
		require
			Call_Is_Procedure_Call: a_call.is_procedure_call
		local
			l_module_name:READABLE_STRING_GENERAL
			l_arguments:CHAIN[detachable ANY]
		do
			if attached a_call.children.at ("name") as la_name then
				if attached la_name.token as la_token then
					l_module_name := la_token.text
					if attached system_modules.at (l_module_name) as la_module then
						if la_module.is_procedure then
							if
								attached a_call.children.at ("arguments") as la_arguments_node and then
								attached expressions_to_chain(la_arguments_node) as la_arguments and then
								(not is_module_requested and not has_error)
							then
								l_arguments := la_arguments
							else
								create {FIXED_LIST[detachable ANY]}l_arguments.make(0)
							end
							if not is_module_requested and not has_error then
								requested_module := [False, la_module, l_arguments]
								last_module_call := a_call
								is_module_requested := True
							end

						elseif la_module.is_function then
							error.set_function_used_in_procedure_call(la_name, l_module_name)
						else
							error.set_internal_error_with_node (a_call)
						end
					else
						error.set_module_not_found(la_name, l_module_name)
					end
				end
			else
				error.set_internal_error_with_node (a_call)
			end
		end

	manage_for_statement(a_statement:RYTHME_AST_NODE)
			-- Initialize a "for" loop and adjust the `to_execute' execution {STACK} to manage the execution of the loop
			-- Warning, can set the `is_module_requested'
		require
			Is_for_Statement_Valid: a_statement.is_for
		local
			l_base_value:NUMERIC
		do
			if attached a_statement.children.at ("for_loop") as la_for_loop then
				if
					attached la_for_loop.children.at ("variable_name") as la_variable_name and
					attached la_for_loop.children.at ("base_expression") as la_base_expression
				then
					if attached expression_value (la_base_expression) as la_base_value then
						if not is_module_requested and not has_error then
							l_base_value := to_numeric (la_base_value)
							if not has_error then
								manage_for_iteration (la_for_loop, l_base_value)
							end
						end
					else
						if not is_module_requested and not has_error then
							error.set_numeric_not_valid_with_node (la_base_expression)
						end
					end
				else
					error.set_internal_error_with_node (a_statement)
				end
			else
				error.set_internal_error_with_node (a_statement)
			end
		end

	manage_for_loop_statement(a_statement:RYTHME_AST_NODE)
			-- Adjust the `to_execute' execution {STACK} according to the "for" control struction in `a_statement'
			-- Warning, can set the `is_module_requested'
		require
			Is_for_Loop_Statement_Valid: a_statement.is_for_loop
		local
			l_step, l_variable:NUMERIC
		do
			if
				attached a_statement.children.at ("variable_name") as la_variable_name and
				attached a_statement.children.at ("base_expression") as la_base_expression and
				attached a_statement.children.at ("limit_expression") as la_limit_expression and
				attached a_statement.children.at ("statements") as la_statements
			then
				l_step := for_loop_step(la_base_expression, la_limit_expression, a_statement.children.at ("step"))
				if
					not is_module_requested and not has_error and then
					attached variable_expression_value (la_variable_name) as la_variable_value and then
					not has_error
				then
					l_variable := to_numeric (la_variable_value)
					if has_error then
						error.set_line_and_column (la_variable_name)
					else
						l_variable := expression_plus_value(l_variable, l_step)
						manage_for_iteration(a_statement, l_variable)
					end
				end

			else
				error.set_internal_error_with_node (a_statement)
			end
		end

	manage_for_iteration(a_statement:RYTHME_AST_NODE; a_variable_value:NUMERIC)
			-- Modify the `to_execute' execution stack depending of the for loop state
			-- Warning, can set the `is_module_requested'
		local
			l_limit, l_base:NUMERIC
		do
			if
				attached a_statement.children.at ("variable_name") as la_variable_name and
				attached a_statement.children.at ("base_expression") as la_base_expression and
				attached a_statement.children.at ("limit_expression") as la_limit_expression
			then
				set_variable_value (la_variable_name, a_variable_value)
				if not is_module_requested and not has_error then
					if attached expression_value (la_base_expression) as la_base_value then
						if not is_module_requested then
							l_base := to_numeric (la_base_value)
							if has_error then
								error.set_line_and_column (la_base_expression)
							else
								if
									attached expression_value (la_limit_expression) as la_limit_value and then
									not is_module_requested
								then
									l_limit := to_numeric (la_limit_value)
									if
										not has_error and then
										((expression_is_greater (l_base, l_limit) and expression_is_greater_or_equal (a_variable_value, l_limit)) or
										(expression_is_lower_or_equal (l_base, l_limit) and expression_is_lower_or_equal (a_variable_value, l_limit)))
									then
										if attached a_statement.children.at ("statements") as la_statements then
											to_execute.extend (a_statement)
											to_execute.extend (la_statements)
										else
											error.set_internal_error_with_node (a_statement)
										end
									end
									if has_error then
										error.set_line_and_column (la_limit_expression)
									end
								else
									if not is_module_requested and not has_error then
										error.set_numeric_not_valid_with_node (la_limit_expression)
									end
								end
							end
						end

					else
						if not is_module_requested and not has_error then
							error.set_numeric_not_valid_with_node (la_base_expression)
						end
					end
				end
			else
				error.set_internal_error_with_node (a_statement)
			end


		end

	for_loop_step(a_base_expression, a_limit_expression:RYTHME_AST_NODE;a_step:detachable RYTHME_AST_NODE):NUMERIC
			-- The step that a for loop must use at each iteration
			-- Warning, can set the `is_module_requested'
		require
			Expressions_Valid: a_base_expression.is_expression and a_limit_expression.is_expression
		do
			Result := 0
			if attached a_step then
				if attached a_step.children.at("expression") as la_step_expression then
					if
						attached expression_value (la_step_expression) as la_step_value and then
						not is_module_requested and not has_error
					then
						Result := to_numeric (la_step_value)
					end
					if has_error then
						error.set_line_and_column (a_step)
					end
				else
					error.set_internal_error_with_node (a_step)
				end
			else
				Result := default_for_loop_step(a_base_expression, a_limit_expression)
			end
		ensure
			Return_Value_Valid: not has_error implies Result /= 0
		end


	default_for_loop_step(a_base_expression, a_limit_expression:RYTHME_AST_NODE):NUMERIC
			-- Look if the base is upper or lower than a_limite_expression and create a
			-- step in that sens.
			-- Warning, can set the `is_module_requested'
		require
			Expressions_Valid: a_base_expression.is_expression and a_limit_expression.is_expression
		local
			l_base, l_limit:NUMERIC
		do
			Result := 0
			if attached expression_value(a_base_expression) as la_base_value then
				if not is_module_requested and not has_error then
					if attached expression_value(a_limit_expression) as la_limit_value then
						if not is_module_requested and not has_error then
							l_limit := to_numeric (la_limit_value)
							if has_error then
								error.set_line_and_column (a_limit_expression)
							else
								l_base := to_numeric (la_base_value)
								if not has_error then
									if expression_is_greater (l_base, l_limit) then
										Result := -1
									else
										Result := 1
									end
								end
								if has_error then
									error.set_line_and_column (a_base_expression)
								end
							end
						end
					else
						if not is_module_requested and not has_error  then
							error.set_numeric_not_valid_with_node (a_limit_expression)
						end
					end
				end
			else
				if not is_module_requested and not has_error then
					error.set_numeric_not_valid_with_node (a_base_expression)
				end
			end
		ensure
			Return_Value_Valid: not has_error implies Result /= 0
		end


	manage_while_and_until_statement(a_statement:RYTHME_AST_NODE)
			-- Adjust the `to_execute' execution {STACK} according to the "while" or "until" control struction in `a_statement'
			-- Warning, can set the `is_module_requested'
		require
			Is_While_Or_Until_Statement_Valid: a_statement.is_while or a_statement.is_until
		local
			l_condition_value:BOOLEAN
		do
			if attached a_statement.children.at ("expression") as la_expression then
				if attached expression_value (la_expression) as la_expression_value then
					if not is_module_requested then
						l_condition_value := to_boolean (la_expression_value)
						if not has_error then
							if a_statement.is_until then
								l_condition_value := not l_condition_value
							end
							if l_condition_value then
								if attached a_statement.children.at ("statements") as la_statements then
									to_execute.extend (a_statement)
									to_execute.extend (la_statements)
								else
									error.set_internal_error_with_node (a_statement)
								end
							end
						end
					end
				else
					if not is_module_requested and not has_error then
						error.set_boolean_not_valid_with_value (void_text)
						error.set_line_and_column (la_expression)
					end

				end
			else
				error.set_internal_error_with_node (a_statement)
			end
		end


	manage_repeat_statement(a_statement:RYTHME_AST_NODE)
			-- Adjust the `to_execute' execution {STACK} according to the "repeat" control struction in `a_statement'
			-- Warning, can set the `is_module_requested'
		require
			Is_Repeat_Statement_Valid: a_statement.is_repeat
		do
			if
				attached a_statement.children.at ("statements") as la_statements and then
				attached a_statement.children.at ("condition") as la_condition
			then
				to_execute.extend (la_condition)
				to_execute.extend (la_statements)
			else
				error.set_internal_error_with_node (a_statement)
			end
		end

	manage_if_statement(a_statement:RYTHME_AST_NODE)
			-- Adjust the `to_execute' execution {STACK} according to the "if" control struction in `a_statement'
			-- Warning, can set the `is_module_requested'
		require
			Is_If_Statement_Valid: a_statement.is_if or a_statement.is_elseif
		local
			l_condition_value:BOOLEAN
		do
			if attached a_statement.children.at ("expression") as la_expression then
				if attached expression_value (la_expression) as la_expression_value then
					if not is_module_requested then
						l_condition_value := to_boolean (la_expression_value)
						if not has_error then
							if l_condition_value then
								if attached a_statement.children.at ("statements") as la_statements then
									to_execute.extend (la_statements)
								else
									error.set_internal_error_with_node (a_statement)
								end
							else
								if attached a_statement.children.at ("else") as la_else then
									if la_else.is_elseif then
										manage_if_statement(la_else)
									elseif la_else.is_else then
										if attached la_else.children.at ("statements") as la_statements then
											to_execute.extend (la_statements)
										else
											error.set_internal_error_with_node (la_else)
										end
									else
										error.set_internal_error_with_node (la_else)
									end
								end
							end
						end
					end
				else
					if not is_module_requested and not has_error then
						error.set_boolean_not_valid_with_value (void_text)
						error.set_line_and_column (la_expression)
					end
				end
			else
				error.set_internal_error_with_node (a_statement)
			end
		end

	execute_write(a_statement:RYTHME_AST_NODE)
			-- Execute a 'Write' statement. Print a value on the screen.
		require
			Statement_Is_Write_Statement: a_statement.is_write
		do
			execute_write_without_end_line(a_statement)
			if not has_error then
				if attached a_statement.children.at ("variable") as la_variable and then la_variable.is_variable then
					if attached {PLAIN_TEXT_FILE}variable_expression_value (la_variable) as la_file and then la_file.is_open_write then
						la_file.put_new_line
					else
						error.set_file_not_valid (la_variable)
					end
				else
					print_agent.call ("%N")
				end
			end
		end

	execute_write_without_end_line(a_statement:RYTHME_AST_NODE)
			-- Execute a 'Write' statement or similar. Print a value on the screen without an end line.
			-- Warning, can set the `is_module_requested'
		require
			Statement_Is_Write_Statement: a_statement.is_write or a_statement.is_read
		do
			if
				attached expressions_to_chain(a_statement) as la_expressions and then
				(not is_module_requested and not has_error)
			then
				if attached a_statement.children.at ("variable") as la_variable and then la_variable.is_variable then
					if attached {PLAIN_TEXT_FILE}variable_expression_value (la_variable) as la_file and then la_file.is_open_write then
						across la_expressions as la_value loop
							la_file.put_string (to_text(la_value.item).to_string_8)
						end
					else
						error.set_file_not_valid (la_variable)
					end
				else
					across la_expressions as la_value loop
						print_agent.call (to_text(la_value.item))
					end
				end
			end
		end

	execute_assignment(a_statement:RYTHME_AST_NODE)
			-- Execute an 'Assignment' statement. Put a value in a variable.
			-- Warning, can set the `is_module_requested'
		require
			Statement_Is_Assignment_Statement: a_statement.is_assignment
		local
			l_value:detachable ANY
		do
			if
				attached a_statement.children.at ("expression") as la_expression and
				attached a_statement.children.at ("variable") as la_variable_id
			then
				if la_expression.is_read then
					create {STRING_32}l_value.make_from_separate (read_value(la_expression))
				else
					l_value := expression_value(la_expression)
				end
				if not is_module_requested and not has_error then
					if la_variable_id.is_variable then
						set_variable_value(la_variable_id, l_value)
					else
						error.set_internal_error_with_node (a_statement)
					end
				end
			else
				error.set_internal_error_with_node (a_statement)
			end
		end

	expression_value(a_expression:RYTHME_AST_NODE):detachable ANY
			-- Calculate the value of `a_expression'
			-- Warning, can set the `is_module_requested'
		require
			Expression_Valid: a_expression.is_expression
		do
			if a_expression.is_variable then
				Result := variable_expression_value(a_expression)
			elseif a_expression.is_primitive then
				Result := primitive_expression_value(a_expression)
			elseif a_expression.is_list then
				Result := expressions_to_chain (a_expression)
			elseif a_expression.is_function_call then
				Result := function_expression_value(a_expression)
			elseif a_expression.is_uniary_expression then
				Result := uniary_expression_value(a_expression)
			elseif a_expression.is_binary_expression then
				Result := binary_expression_value(a_expression)
			elseif a_expression.is_lazy_evaluated_binary_expression then
				Result := lazy_evaluated_binary_expression_value(a_expression)
			elseif a_expression.is_type_checker then
				Result := type_check_expression_value(a_expression)
			else
				create Result
				error.set_expression_not_valid_with_node(a_expression)
			end
		end


	type_check_expression_value(a_expression:RYTHME_AST_NODE):BOOLEAN
			-- Return True if the type check in `a_expression' succeed
			-- Warning, can set the `is_module_requested'
		local
			l_expression_value:detachable ANY
		do
			if
				attached a_expression.children.at ("expression") as la_expression and
				attached a_expression.children.at ("type") as la_type
			then
				l_expression_value := expression_value (la_expression)
				if not is_module_requested and not has_error then
					if attached la_type.token as la_token then
						if attached l_expression_value then
							if la_token.is_boolean_type then
								Result := can_evaluate_to_boolean(l_expression_value)
							elseif la_token.is_integer_type then
								Result := can_evaluate_to_integer (l_expression_value)
							elseif la_token.is_real_type then
								Result := can_evaluate_to_real(l_expression_value)
							elseif la_token.is_list_type then
								Result := attached {CHAIN[detachable ANY]} l_expression_value
							elseif la_token.is_file_type then
								Result := attached {FILE} l_expression_value
							elseif la_token.is_void then
								Result := False
							else
								error.set_internal_error_with_node (a_expression)
							end
						else
							Result := la_token.is_void
						end
					else
						error.set_internal_error_with_node (a_expression)
					end
				end
			end
		end

	binary_expression_value(a_expression:RYTHME_AST_NODE):ANY
			-- The value evaluate by a binary `a_expression'
			-- Warning, can set the `is_module_requested'
		require
			Expression_Valid: a_expression.is_binary_expression
		local
			l_expression_value_1, l_expression_value_2:detachable ANY
		do
			create Result
			a_expression.children.search ("expression_1")
			if a_expression.children.found and then attached a_expression.children.found_item as la_expression_1 then
				a_expression.children.search ("expression_2")
				if a_expression.children.found and then attached a_expression.children.found_item as la_expression_2 then
					l_expression_value_1 := expression_value(la_expression_1)
					if not has_error then
						l_expression_value_2 := expression_value(la_expression_2)
						if not has_error then
							if not is_module_requested then
								if attached a_expression.token as la_token then
									Result := binary_value_with_token(la_token, l_expression_value_1, l_expression_value_2)
									if has_error then
										error.set_line_and_column (a_expression)
									end
								else
									create Result
									error.set_expression_not_valid_with_node(a_expression)
								end
							end
						else
							error.set_line_and_column (a_expression)
						end
					else
						error.set_line_and_column (a_expression)
					end

				else
					create Result
					error.set_expression_not_valid_with_node(a_expression)
				end
			else
				create Result
				error.set_expression_not_valid_with_node(a_expression)
			end
		end

	binary_value_with_token(a_token:RYTHME_TOKEN; a_expression_value_1, a_expression_value_2:detachable ANY):ANY
			-- Get the value of a binary operation. `a_token' tell operation and
			-- `a_expression_value_1' and `a_expression_value_2' are the values to operate
		do
			if attached a_expression_value_1 and then attached a_expression_value_2 then
				if a_token.is_plus then
					Result := expression_plus_value(to_numeric(a_expression_value_1), to_numeric(a_expression_value_2))
				elseif a_token.is_minus then
					Result := expression_minus_value(to_numeric(a_expression_value_1), to_numeric(a_expression_value_2))
				elseif a_token.is_star then
					Result := expression_star_value(to_numeric(a_expression_value_1), to_numeric(a_expression_value_2))
				elseif a_token.is_slash then
					Result := expression_slash_value(to_numeric(a_expression_value_1), to_numeric(a_expression_value_2))
				elseif a_token.is_div then
					Result := 	normalized_integer_value(to_numeric(a_expression_value_1)) //
								normalized_integer_value(to_numeric(a_expression_value_2))
				elseif a_token.is_mod then
					Result := 	normalized_integer_value(to_numeric(a_expression_value_1)) \\
								normalized_integer_value(to_numeric(a_expression_value_2))
				elseif a_token.is_lshift then
					Result := 	normalized_integer_value(to_numeric(a_expression_value_1)) |<<
								normalized_integer_value(to_numeric(a_expression_value_2)).to_integer_32
				elseif a_token.is_rshift then
					Result := 	normalized_integer_value(to_numeric(a_expression_value_1)) |<<
								normalized_integer_value(to_numeric(a_expression_value_2)).to_integer_32
				elseif a_token.is_power then
					Result := expression_power_value(to_numeric(a_expression_value_1), to_numeric(a_expression_value_2))
				elseif a_token.is_eq then
					Result := a_expression_value_1.out.same_string (a_expression_value_2.out)
				elseif a_token.is_ne then
					Result := not a_expression_value_1.out.same_string (a_expression_value_2.out)
				elseif a_token.is_gt then
					Result := expression_is_greater(to_numeric(a_expression_value_1), to_numeric(a_expression_value_2))
				elseif a_token.is_lt then
					Result := expression_is_lower(to_numeric(a_expression_value_1), to_numeric(a_expression_value_2))
				elseif a_token.is_ge then
					Result := expression_is_greater_or_equal(to_numeric(a_expression_value_1), to_numeric(a_expression_value_2))
				elseif a_token.is_le then
					Result := expression_is_lower_or_equal(to_numeric(a_expression_value_1), to_numeric(a_expression_value_2))
				elseif a_token.is_xor then
					Result := to_boolean(a_expression_value_1) xor to_boolean(a_expression_value_2)
				else
					create Result
					error.set_expression_not_valid
				end
			else
				if a_token.is_eq then
					Result := not (attached a_expression_value_1 or attached a_expression_value_2)
				elseif a_token.is_ne then
					Result := attached a_expression_value_1 or attached a_expression_value_2
				else
					create Result
					error.set_operator_used_on_void_variable_with_token(a_token)
				end
			end

		end

	lazy_evaluated_binary_expression_value(a_expression:RYTHME_AST_NODE):BOOLEAN
			-- The value evaluate by a binary `a_expression' that is lazy evaluated
			-- Warning, can set the `is_module_requested'
		require
			Expression_Valid: a_expression.is_lazy_evaluated_binary_expression
		do
			a_expression.children.search ("expression_1")
			if a_expression.children.found and then attached a_expression.children.found_item as la_expression_1 then
				a_expression.children.search ("expression_2")
				if a_expression.children.found and then attached a_expression.children.found_item as la_expression_2 then
					if attached a_expression.token as la_token then
						if attached expression_value(la_expression_1) as la_value_1 then
							if la_token.is_and then
								if to_boolean(la_value_1) then
									if attached expression_value(la_expression_2) as la_value_2 then
										Result := to_boolean(la_value_2)
									else
										error.set_boolean_not_valid
										error.set_line_and_column (la_expression_2)
									end
								else
									Result := False
								end
							elseif la_token.is_or then
								if to_boolean(la_value_1) then
									Result := True
								else
									if attached expression_value(la_expression_2) as la_value_2 then
										Result := to_boolean(la_value_2)
									else
										error.set_boolean_not_valid
										error.set_line_and_column (la_expression_2)
									end
								end
							elseif la_token.is_implies then
								if to_boolean(la_value_1) then
									if attached expression_value(la_expression_2) as la_value_2 then
										Result := to_boolean(la_value_2)
									else
										error.set_boolean_not_valid
										error.set_line_and_column (la_expression_2)
									end
								else
									Result := True
								end
							else
								error.set_expression_not_valid_with_node(a_expression)
							end
						else
							error.set_boolean_not_valid
							error.set_line_and_column (la_expression_1)
						end
					else
						create Result
						error.set_expression_not_valid_with_node(a_expression)
					end
				else
					create Result
					error.set_expression_not_valid_with_node(a_expression)
				end
			else
				create Result
				error.set_expression_not_valid_with_node(a_expression)
			end
		end

	uniary_expression_value(a_expression:RYTHME_AST_NODE):detachable ANY
			-- Value of the uniary `a_expression'
			-- Warning, can set the `is_module_requested'
		require
			Expression_Valid: a_expression.is_uniary_expression
		local
			l_expression_value:detachable ANY
		do
			create Result
			a_expression.children.search ("expression")
			if a_expression.children.found and then attached a_expression.children.found_item as la_expression then
				l_expression_value := expression_value(la_expression)
				if not is_module_requested then
					if attached a_expression.token as la_token then
						if la_token.is_minus then
							Result := expression_uniary_minus_value(l_expression_value)
						elseif la_token.is_not then
							Result := expression_uniary_not_value(l_expression_value)
						elseif la_token.is_lparan then
							Result := l_expression_value
						else
							create Result
							error.set_expression_not_valid_with_node (a_expression)
						end
						if has_error then
							error.set_line_and_column (a_expression)
						end
					else
						create Result
						error.set_expression_not_valid_with_node (a_expression)
					end
				end
			else
				create Result
				error.set_expression_not_valid_with_node (a_expression)
			end
		end



	primitive_expression_value(a_expression:RYTHME_AST_NODE):detachable ANY
			-- Value of the primitive `a_expression'
		require
			Expression_Valid: a_expression.is_primitive
			Token_Exist: attached a_expression.token
		do
			if attached a_expression.token as la_token then
				if la_token.is_string then
					Result := la_token.text.substring (2, la_token.text.count-1)
				elseif la_token.is_integer then
					if la_token.text.is_integer_64 then
						Result := la_token.text.to_integer_64
					else
						Result := 0
						error.set_syntax_not_valid_with_node(a_expression)
					end
				elseif la_token.is_real then
					if la_token.text.is_real_64 then
						Result := la_token.text.to_real_64
					else
						Result := 0
						error.set_syntax_not_valid_with_node(a_expression)
					end
				elseif la_token.is_true then
					Result := True
				elseif la_token.is_false then
					Result := False
				elseif la_token.is_void then
					Result := Void
				else
					error.set_internal_error_with_node (a_expression)
				end
			else
				error.set_internal_error_with_node (a_expression)
			end
		end

	variable_expression_value(a_expression:RYTHME_AST_NODE):detachable ANY
			-- The value associated with a variable ID represented in `a_expression'
			-- Warning, can set the `is_module_requested'
		require
			Expression_Valid: attached a_expression as la_expression and then la_expression.is_variable
		local
			l_variable_name:READABLE_STRING_GENERAL
		do
			if a_expression.is_list then
				Result := list_expression_value(a_expression)
			else
				if attached a_expression.token as la_token then
					l_variable_name := la_token.text
					variables.search (l_variable_name)
					if variables.found then
						Result := variables.found_item
					else
						error.set_variable_not_found_with_value (a_expression, l_variable_name)
					end
				else
					error.set_expression_not_valid_with_node (a_expression)
				end
			end

		end

	list_expression_value(a_expression:RYTHME_AST_NODE):detachable ANY
			-- The value associated with a list represented in `a_expression'
			-- Warning, can set the `is_module_requested'
		require
			Expression_Valid: attached a_expression as la_expression and then la_expression.is_list
		local
			l_list_name:READABLE_STRING_GENERAL
			l_index_value:NUMERIC
			l_index:INTEGER_64
		do
			if attached a_expression.token as la_token then
				l_list_name := la_token.text
				if
					attached {CHAIN[detachable ANY]} variables.at (l_list_name) as la_list and
					(attached a_expression.children.at ("index") as la_index_expression and then
					la_index_expression.is_expression)
				then
					if attached expression_value(la_index_expression) as la_value then
						if not is_module_requested then
							l_index_value := to_numeric(la_value)
							if not has_error and then is_value_integer (l_index_value) then
								l_index := normalized_integer_value(l_index_value)
								if l_index > 0  and l_index <= la_list.count then
									Result := la_list.at (l_index.to_integer_32)
								else
									error.set_list_index_not_valid_with_value (la_index_expression, l_index.out)
								end
							else
								error.set_numeric_not_valid_with_node(la_index_expression)
							end
						end
					else
						if not is_module_requested and not has_error then
							error.set_list_index_not_valid_with_value (la_index_expression, void_text)
						end
					end

				else
					error.set_variable_not_found_with_value (a_expression, l_list_name)
				end
			else
				error.set_expression_not_valid_with_node (a_expression)
			end
		end

	function_expression_value(a_expression:RYTHME_AST_NODE):detachable ANY
			-- The value after executing the function whose ID is in `a_expression'
			-- Warning, can set the `is_module_requested'
		require
			Expression_Valid: attached a_expression as la_expression and then la_expression.is_function_call
		do
			if attached a_expression.token as la_token then
				if attached expressions_to_chain(a_expression) as la_arguments then
					if not is_module_requested then
						if
							attached system_modules.at (la_token.text) as la_function and then
							la_function.is_function
						then
							Result := function_module_expression_value(a_expression, la_function, la_arguments)
						elseif attached functions.at(la_token.text) as la_function then
							Result := la_function.item ([error,la_arguments])
							if has_error then
								error.set_line_and_column (a_expression)
							end
						else
							create Result
							error.set_function_not_found_with_value (a_expression, la_token.text)
						end
					end

				else
					create Result
					error.set_expression_not_valid_with_node (a_expression)
				end
			else
				create Result
				error.set_expression_not_valid_with_node (a_expression)
			end
		end

	function_module_expression_value(a_expression, a_function:RYTHME_AST_NODE; a_arguments:CHAIN[detachable ANY]):detachable ANY
			-- The value after executing the module function `a_function' with `a_arguments' requested by `a_expression'
			-- If the value has never been requested before, return `Void ' and set `is_module_requested'.
			-- This structure is done to add a "step into" functionnality in a debugger
			-- Warning, can set the `is_module_requested'
		local
			is_found:BOOLEAN
		do
			from
				called_functions_returned_value.start
				is_found := False
			until
				called_functions_returned_value.exhausted or is_found
			loop
				if called_functions_returned_value.item.node = a_expression then
					Result := called_functions_returned_value.item.value
					is_found := True
				end
				called_functions_returned_value.forth
			end
			if not is_found then
				requested_module := [True, a_function, a_arguments]
				last_module_call := a_expression
				is_module_requested := True
			end
		end

	manage_function_result(la_result_expression:RYTHME_AST_NODE)
			-- If `Current' is a function module, set the `return_value' accordingly
		require
			Result_Expression_Valid: la_result_expression.is_result and la_result_expression.is_expression
		local
			l_return_value:detachable ANY
		do
			l_return_value := expression_value (la_result_expression)
			if not is_module_requested then
				return_value := l_return_value
				returned_value_action.call ([l_return_value])
			end

		end

	set_variable_value(a_variable_node:RYTHME_AST_NODE; a_value:detachable ANY)
			-- Assign the value of the variable identified in `a_variable_node'
			-- Warning, can set the `is_module_requested'
		require
			Is_Valid_Variable_Node: a_variable_node.is_variable
		do
			if a_variable_node.is_list then
				set_list_value(a_variable_node,a_value)
			else
				if attached a_variable_node.token as la_token then
					variables.force (a_value, la_token.text)
				else
					error.set_internal_error_with_node (a_variable_node)
				end
			end
		end

	set_list_value(a_list_node:RYTHME_AST_NODE; a_value:detachable ANY)
			-- Assign `a_value' to the list element included in `a_list_node'
			-- Warning, can set the `is_module_requested'
		require
			Is_Valid_List_Node: a_list_node.is_list
		local
			l_index:INTEGER_64
		do
			if
				attached a_list_node.token as la_token and then
				attached a_list_node.children.at ("index") as la_index
			then
				if attached variables.at (la_token.text) as la_variable then
					if attached {CHAIN[detachable ANY]}la_variable as la_list then
						if attached expression_value (la_index) as la_expression_value then
							if not is_module_requested then
								l_index := normalized_integer_value (to_numeric (la_expression_value))
								if not has_error then
									if l_index > 0 and l_index <= la_list.count then
										la_list.put_i_th (a_value, l_index.to_integer_32)
									else
										error.set_list_index_not_valid_with_value (a_list_node, l_index.out)
									end
								end
							end
						else
							if not is_module_requested and not has_error then
								error.set_list_index_not_valid_with_value (a_list_node, void_text)
							end
						end
					else
						error.set_list_not_valid (a_list_node, la_token.text)
					end
				else
					error.set_variable_not_found_with_value (a_list_node, la_token.text)
				end
			else
				error.set_internal_error_with_node (a_list_node)
			end
		end

	read_value(a_read:RYTHME_AST_NODE):READABLE_STRING_GENERAL
			-- Ask a request question (optionnal) and read a value on the screen.
		require
			Read_Node_Is_Read_Statement: a_read.is_read
		do
			if attached a_read.children.at ("expression") then
				execute_write_without_end_line (a_read)
			end
			if not has_error then
				if attached a_read.children.at ("variable") as la_variable then
					if attached {PLAIN_TEXT_FILE}variable_expression_value (la_variable) as la_file then
						la_file.read_line
						Result := la_file.last_string
					else
						Result := ""
						error.set_file_not_valid (la_variable)
					end
				else
					Result := read_agent.item ([])
				end


			else
				Result := ""
			end
		end


	expressions_to_chain(a_expressions:RYTHME_AST_NODE):LINKED_LIST[detachable ANY]
			-- A {CHAIN} of values getted from every expression in `a_expressions'
			-- Warning, can set the `is_module_requested'
		do
			if not is_module_requested and attached a_expressions then
				if attached a_expressions.children.at ("expression") as la_expression then
					if attached a_expressions.children.at ("expressions") as la_next_expressions then
						Result := expressions_to_chain(la_next_expressions)
					else
						create Result.make
					end
					Result.put_front (expression_value(la_expression))
				else
					create Result.make
				end
			else
				create Result.make
			end
		end

	set_module_name(a_ast:RYTHME_AST_NODE)
			-- Assign the `module_name' to the one of the syntax tree `a_ast'
		do
			a_ast.children.search ("name")
			if a_ast.children.found then
				if attached a_ast.children.at("name") as la_name then
					if attached la_name.token as la_token then
						check la_token.is_id end
						module_name := la_token.text
					else
						module_name := ""
					end
				else
					module_name := ""
				end
			else
				module_name := "main"
			end
		end

	generate_module_argument(a_arguments_node:RYTHME_AST_NODE; a_arguments_list:CHAIN[detachable ANY]; a_index:INTEGER):STRING_TABLE[detachable ANY]
			-- Using the `a_arguments_node' containing the arguments definition and `a_arguments'
			-- containing the arguments value, generate a table of arguments to use in module call
		do
			create Result.make (1)
			if attached a_arguments_node.children.at ("name") as la_name then
				check la_name.is_variable end
				if attached la_name.token as la_token then
					if a_index > a_arguments_list.count then
						if attached last_module_call as la_module_call then
							error.set_module_argument_count_not_valid(la_module_call)
						else
							error.set_internal_error
						end

					else
						Result.extend (a_arguments_list.at(a_index), la_token.text)
					end
				else
					error.set_internal_error_with_node (la_name)
				end
				if not has_error then
					if attached a_arguments_node.children.at ("arguments") as la_next then
						Result.merge (generate_module_argument(la_next, a_arguments_list, a_index + 1))
					else
						if a_index /= a_arguments_list.count then
							if attached last_module_call as la_last_module_call then
								error.set_module_argument_count_not_valid (la_last_module_call)
							else
								error.set_internal_error
							end

						end
					end
				end

			else
				error.set_internal_error_with_node (a_arguments_node)
			end
		end

	default_print(a_str:READABLE_STRING_GENERAL)
			-- The default print routine.
		do
			io.standard_default.put_string(a_str.as_string_8)
		end

	default_read:READABLE_STRING_GENERAL
			-- The default read routine
		do
			io.input.read_line
			Result := io.input.last_string
		end

invariant
	Module_Call_Valid: is_module_requested implies (attached last_module_call and attached requested_module)
end
