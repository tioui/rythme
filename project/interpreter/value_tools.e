note
	description: "Summary description for {VALUE_TOOLS}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	VALUE_TOOLS

feature -- Access

	error: VALUE_TOOLS_ERROR
			-- Contain the last error encounter by `Current'

	true_text:READABLE_STRING_GENERAL assign set_true_text
			-- Text representing the True value

	set_true_text(a_text:READABLE_STRING_GENERAL)
			-- Assign `a_text' to `true_text'
		do
			true_text := a_text
		end

	false_text:READABLE_STRING_GENERAL assign set_false_text
			-- Text representing the False value

	set_false_text(a_text:READABLE_STRING_GENERAL)
			-- Assign `a_text' to `false_text'
		do
			false_text := a_text
		end

	void_text:READABLE_STRING_GENERAL assign set_void_text
			-- Text representing the value of a Void item

	set_void_text(a_text:READABLE_STRING_GENERAL)
			-- Assign `a_text' to `void_text'
		do
			void_text := a_text
		end

	file_text:READABLE_STRING_GENERAL assign set_file_text
			-- Text representing the value of a File item

	set_file_text(a_text:READABLE_STRING_GENERAL)
			-- Assign `a_text' to `file_text'
		do
			file_text := a_text
		end

feature {NONE} -- Implementation

	expression_is_lower(a_expression_value_1, a_expression_value_2:NUMERIC):BOOLEAN
			-- True if `a_expression_value_1' is lower than `a_expression_value_2'
		local
			l_comparables:TUPLE[value_1, value_2:COMPARABLE]
		do
			l_comparables := to_same_comparable(a_expression_value_1, a_expression_value_2)
			Result := l_comparables.value_1 < l_comparables.value_2
		end

	expression_is_greater(a_expression_value_1, a_expression_value_2:NUMERIC):BOOLEAN
			-- True if `a_expression_value_1' is greater than `a_expression_value_2'
		local
			l_comparables:TUPLE[value_1, value_2:COMPARABLE]
		do
			l_comparables := to_same_comparable(a_expression_value_1, a_expression_value_2)
			Result := l_comparables.value_1 > l_comparables.value_2
		end

	expression_is_lower_or_equal(a_expression_value_1, a_expression_value_2:NUMERIC):BOOLEAN
			-- True if `a_expression_value_1' is lower or equal than `a_expression_value_2'
		local
			l_comparables:TUPLE[value_1, value_2:COMPARABLE]
		do
			l_comparables := to_same_comparable(a_expression_value_1, a_expression_value_2)
			Result := l_comparables.value_1 <= l_comparables.value_2
		end

	expression_is_greater_or_equal(a_expression_value_1, a_expression_value_2:NUMERIC):BOOLEAN
			-- True if `a_expression_value_1' is greater or equal than `a_expression_value_2'
		local
			l_comparables:TUPLE[value_1, value_2:COMPARABLE]
		do
			l_comparables := to_same_comparable(a_expression_value_1, a_expression_value_2)
			Result := l_comparables.value_1 >= l_comparables.value_2
		end

	to_same_comparable(a_value_1, a_value_2:NUMERIC):TUPLE[value_1, value_2:COMPARABLE]
			-- Transform `a_value_1' and `a_value_2' to make them comparable (same type)
		do
			if is_value_integer (a_value_1) and is_value_integer (a_value_2) then
				Result := [normalized_integer_value (a_value_1), normalized_integer_value (a_value_2)]
			elseif is_value_real (a_value_1) or is_value_real (a_value_2) then
				Result := [normalized_real_value (a_value_1),  normalized_real_value(a_value_2)]
			else
				Result := [0,0]
				error.set_cannot_compare_with_value (a_value_1.out + ", " + a_value_2.out)
			end
		end

	normalize_numerics(a_value_1, a_value_2:NUMERIC):TUPLE[value_1, value_2:NUMERIC]
			-- Transform `a_value_1' and `a_value_2' to make them to the same type
		do
			if is_value_integer (a_value_1) and is_value_integer (a_value_2) then
				Result := [normalized_integer_value (a_value_1), normalized_integer_value (a_value_2)]
			elseif is_value_real (a_value_1) or is_value_real (a_value_2) then
				Result := [normalized_real_value (a_value_1),  normalized_real_value(a_value_2)]
			else
				Result := [0,0]
				error.set_cannot_compare_with_value (a_value_1.out + ", " + a_value_2.out)
			end
		end

	expression_plus_value(a_expression_value_1, a_expression_value_2:NUMERIC):NUMERIC
			-- The value of `a_expression_value_1' plus `a_expression_value_2'
		local
			l_values: TUPLE[value_1, value_2:NUMERIC]
		do
			l_values := normalize_numerics(a_expression_value_1, a_expression_value_2)
			Result := l_values.value_1 + l_values.value_2
		end

	expression_minus_value(a_expression_value_1, a_expression_value_2:NUMERIC):NUMERIC
			-- The value of `a_expression_value_1' minus `a_expression_value_2'
		local
			l_values: TUPLE[value_1, value_2:NUMERIC]
		do
			l_values := normalize_numerics(a_expression_value_1, a_expression_value_2)
			Result := l_values.value_1 - l_values.value_2
		end

	expression_star_value(a_expression_value_1, a_expression_value_2:NUMERIC):NUMERIC
			-- The value of `a_expression_value_1' multiply by `a_expression_value_2'
		local
			l_values: TUPLE[value_1, value_2:NUMERIC]
		do
			l_values := normalize_numerics(a_expression_value_1, a_expression_value_2)
			Result := l_values.value_1 * l_values.value_2
		end

	expression_power_value(a_expression_value_1, a_expression_value_2:NUMERIC):REAL_64
			-- The value of `a_expression_value_1' exponent `a_expression_value_2'
		do
			if is_value_integer (a_expression_value_1) then
				Result := normalized_integer_value(a_expression_value_1) ^ normalized_real_value(a_expression_value_2)
			else
				Result := normalized_real_value(a_expression_value_1) ^ normalized_real_value(a_expression_value_2)
			end
		end

	expression_slash_value(a_expression_value_1, a_expression_value_2:NUMERIC):REAL_64
			-- Value of `a_expression_value_1' divided by `a_expression_value_2'
		do
			if is_value_integer(a_expression_value_1) and is_value_integer(a_expression_value_2) then
				Result := normalized_integer_value(a_expression_value_1) / normalized_integer_value(a_expression_value_2)
			else
				Result := normalized_real_value(a_expression_value_1) / normalized_real_value(a_expression_value_2)
			end
		end

	expression_div_value(a_expression_value_1, a_expression_value_2:NUMERIC):REAL_64
			-- Value of the integer division of `a_expression_value_1' by `a_expression_value_2'
		do
			if is_value_integer(a_expression_value_1) and is_value_integer(a_expression_value_2) then
				Result := normalized_integer_value(a_expression_value_1) / normalized_integer_value(a_expression_value_2)
			else
				Result := normalized_real_value(a_expression_value_1) / normalized_real_value(a_expression_value_2)
			end
		end

	is_value_integer(a_value:NUMERIC):BOOLEAN
			-- True if the type of `a_value' is {INTEGER_X} or {NATURAL_X}
		do
			Result := 	attached {INTEGER_8} a_value or attached {INTEGER_16} a_value or attached {INTEGER_32} a_value or
						attached {INTEGER_64} a_value or attached {NATURAL_8} a_value or attached {NATURAL_16} a_value or
						attached {NATURAL_32} a_value or attached {NATURAL_64} a_value
		end

	is_value_real(a_value:NUMERIC):BOOLEAN
			-- True if the type of `a_value' is {REAL_X}
		do
			Result := attached {REAL_32} a_value or attached {REAL_64} a_value
		end

	can_evaluate_to_integer(a_value:ANY):BOOLEAN
			-- `a_value' can be evaluate as an integer value
		do
			Result := False
			if attached {NUMERIC} a_value as la_value then
				Result := is_value_integer (la_value)
			elseif attached {READABLE_STRING_GENERAL} a_value as la_value then
				Result := la_value.is_integer_64
			end
		end

	can_evaluate_to_real(a_value:ANY):BOOLEAN
			-- `a_value' can be evaluate as an real value
		do
			Result := False
			if attached {NUMERIC} a_value as la_value then
				Result := is_value_integer (la_value) or is_value_real (la_value)
			elseif attached {READABLE_STRING_GENERAL} a_value as la_value then
				Result := la_value.is_real_64
			end
		end

	can_evaluate_to_boolean(a_value:ANY):BOOLEAN
			-- Return True if `a_value' can be evaluated as a boolean value
		do
			Result := False
			if attached {BOOLEAN} a_value then
				Result := True
			elseif attached {READABLE_STRING_GENERAL} a_value as la_value then
				if la_value.same_caseless_characters (true_text, 1, la_value.count, 1) then
					Result := True
				elseif la_value.same_caseless_characters (false_text, 1, la_value.count, 1) then
					Result := True
				end
			end
		end

	normalized_integer_value(a_value:NUMERIC):INTEGER_64
			-- The {INTEGER_64} representation of `a_value'
		do
			if attached {INTEGER_8} a_value as la_value then
				Result := la_value.to_integer_64
			elseif attached {INTEGER_16} a_value as la_value then
				Result := la_value.to_integer_64
			elseif attached {INTEGER_32} a_value as la_value then
				Result := la_value.to_integer_64
			elseif attached {INTEGER_64} a_value as la_value then
				Result := la_value.to_integer_64
			elseif attached {NATURAL_8} a_value as la_value then
				Result := la_value.to_integer_64
			elseif attached {NATURAL_16} a_value as la_value then
				Result := la_value.to_integer_64
			elseif attached {NATURAL_32} a_value as la_value then
				Result := la_value.to_integer_64
			elseif attached {NATURAL_64} a_value as la_value then
				Result := la_value.to_integer_64
			elseif attached {REAL_32} a_value as la_value then
				Result := la_value.truncated_to_integer_64
			elseif attached {REAL_64} a_value as la_value then
				Result := la_value.truncated_to_integer_64
			else
				Result := 0
				error.set_numeric_not_valid_with_value(a_value.out)
			end
		end

	normalized_real_value(a_value:NUMERIC):REAL_64
			-- The {REAL_64} representation of `a_value'
		do
			if attached {INTEGER_8} a_value as la_value then
				Result := la_value.to_double
			elseif attached {INTEGER_16} a_value as la_value then
				Result := la_value.to_double
			elseif attached {INTEGER_32} a_value as la_value then
				Result := la_value.to_double
			elseif attached {INTEGER_64} a_value as la_value then
				Result := la_value.to_double
			elseif attached {NATURAL_8} a_value as la_value then
				Result := la_value.to_real_64
			elseif attached {NATURAL_16} a_value as la_value then
				Result := la_value.to_real_64
			elseif attached {NATURAL_32} a_value as la_value then
				Result := la_value.to_real_64
			elseif attached {NATURAL_64} a_value as la_value then
				Result := la_value.to_real_64
			elseif attached {REAL_32} a_value as la_value then
				Result := la_value.to_double
			elseif attached {REAL_64} a_value as la_value then
				Result := la_value
			else
				Result := 0
				error.set_numeric_not_valid_with_value(a_value.out)
			end
		end

	to_numeric(a_value:ANY):NUMERIC
			-- The numeric representation of `a_value'.
			-- Set an `error' if impossible.
		do
			if attached {NUMERIC} a_value as la_value then
				Result := la_value
			elseif attached {READABLE_STRING_GENERAL} a_value as la_value then
				if la_value.is_integer_64 then
					Result := la_value.to_integer_64
				elseif la_value.is_real_64 then
					Result := la_value.to_integer_64
				else
					Result := 0
					error.set_numeric_not_valid_with_value(la_value)
				end
			else
				Result := 0
				error.set_numeric_not_valid
			end
		end

	to_boolean(a_value:ANY):BOOLEAN
			-- The boolean value retreive fron `a_value'
		do
			if attached {BOOLEAN} a_value as la_value then
				Result := la_value
			elseif attached {READABLE_STRING_GENERAL} a_value as la_value then
				if la_value.same_caseless_characters (true_text, 1, la_value.count, 1) then
					Result := True
				elseif la_value.same_caseless_characters (false_text, 1, la_value.count, 1) then
					Result := False
				else
					Result := False
					error.set_boolean_not_valid_with_value(la_value)
				end
			else
				error.set_boolean_not_valid
			end
		end

	expression_uniary_minus_value(a_value:detachable ANY):ANY
			-- The opposite of a numeric `a_value'
		local
			l_numeric_value:NUMERIC
		do
			if attached {NUMERIC} a_value as la_value then
				l_numeric_value := la_value
			elseif attached {READABLE_STRING_GENERAL} a_value as la_string_value then
				l_numeric_value := to_numeric(la_string_value)
			else
				l_numeric_value := 0
				error.set_numeric_not_valid
			end
			Result := - l_numeric_value
		end

	expression_uniary_not_value(a_value:detachable ANY):ANY
			-- The opposite of a boolean `a_value'
		local
			l_boolean_value:BOOLEAN
		do
			if attached {BOOLEAN} a_value as la_value then
				l_boolean_value := la_value
			elseif attached {READABLE_STRING_GENERAL} a_value as la_string_value then
				l_boolean_value := to_boolean(la_string_value)
			else
				l_boolean_value := False
				error.set_boolean_not_valid
			end
			Result := not l_boolean_value
		end

	to_text(a_value:detachable ANY):READABLE_STRING_GENERAL
			-- A text representation of `a_value'
		do
			if attached {BOOLEAN} a_value as la_boolean_value then
				Result := boolean_to_text (la_boolean_value)
			elseif attached {CHAIN[detachable ANY]} a_value as la_list_value then
				Result := list_to_text(la_list_value)
			elseif attached {FILE} a_value as la_file then
				Result := file_text + "(" + la_file.path.name.to_string_8 + ")"
			elseif attached a_value then
				Result := a_value.out
			else
				Result := void_text
			end
		end

	list_to_text(a_list:CHAIN[detachable ANY]):READABLE_STRING_GENERAL
			-- A text representation of `a_list'
		local
			l_is_first_passed:BOOLEAN
		do
			Result := "{"
			l_is_first_passed := False
			across a_list as la_list loop
				if l_is_first_passed then
					Result := Result + ", "
				else
					l_is_first_passed := True
				end
				Result := Result + to_text(la_list.item)
			end
			Result := Result + "}"
		end

	boolean_to_text(a_boolean:BOOLEAN):READABLE_STRING_GENERAL
			-- The text representation of `a_boolean'
		do
			if a_boolean then
				Result := true_text
			else
				Result := false_text
			end
		end

end
