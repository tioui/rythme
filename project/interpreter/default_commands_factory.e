note
	description: "Default functions found by default in the Rythme language."
	author: "Louis Marchand"
	date: "2014, November 22"
	revision: "v0.1"

class
	DEFAULT_COMMANDS_FACTORY

inherit
	RYTHME_COMMANDS_FACTORY
		redefine
			make_with_constants
		end

create
	default_create,
	make_with_constants

feature {NONE} -- Initialization

	make_with_constants(a_constants:CONSTANTS)
			-- <Precursor>
		do
			Precursor {RYTHME_COMMANDS_FACTORY}(a_constants)
			constants := a_constants
		end

feature -- Access

	functions:STRING_TABLE[FUNCTION[ANY, TUPLE[error:INTERPRETER_ERROR;arguments:CHAIN[detachable ANY]], detachable ANY]]
			-- <Precursor>
		do
			create Result.make(3)
			Result.extend (agent concatenate, constants.concatenation)
			Result.extend ( agent create_list, constants.create_list)
			Result.extend (agent create_file, constants.create_file)
		end

	procedure:STRING_TABLE[PROCEDURE[ANY, TUPLE[error:INTERPRETER_ERROR;arguments:CHAIN[detachable ANY]]]]
			-- <Precursor>
		do
			create Result.make(1)
			Result.extend (agent close_file, constants.close_file)
		end

feature {NONE} -- Implementation

	constants:CONSTANTS
			-- The {CONSTANTS} objet used to initialize `Current'

	concatenate(a_error:INTERPRETER_ERROR;a_arguments:CHAIN[detachable ANY]):READABLE_STRING_GENERAL
			-- Concatenate every value in `a_arguments'.
			-- Set errors in `a_error' if an error occured
			-- Warning: This function is not pure. `a_error' may be modified
		do
			Result := ""
			across a_arguments as la_arguments loop
				Result := Result + 	to_text(la_arguments.item)
			end
			a_error.copy_from (error)
		end

	create_list(a_error:INTERPRETER_ERROR;a_arguments:CHAIN[detachable ANY]):detachable ARRAYED_LIST[detachable ANY]
			-- Create a new list having the count specified in an {INTEGER} specified in `a_arguments'.at(1).
			-- Set errors in `a_error' if an error occured
			-- Warning: This function is not pure. `a_error' may be modified
		local
			l_count:INTEGER
		do
			if a_arguments.count = 1 and then attached a_arguments.at (1) as la_argument then
				l_count := normalized_integer_value(to_numeric (la_argument)).to_integer_32
				if error.exist then
					a_error.copy_from (error)
				else
					create Result.make_filled(l_count)
				end
			else
				a_error.set_argument_count_error
			end
		end

	create_file(a_error:INTERPRETER_ERROR; a_arguments:CHAIN[detachable ANY]):detachable PLAIN_TEXT_FILE
			-- Create a new file at the {STRING} path specified in `a_arguments'.at(1).
			-- Set errors in `a_error' if an error occured
			-- Warning: This function is not pure. `a_error' may be modified
		do
			if a_arguments.count = 1 and then attached a_arguments.at (1) as la_argument then
				if attached {READABLE_STRING_GENERAL}la_argument as la_path then
					create Result.make_with_name (la_path)
					if Result.is_creatable then
						Result.create_read_write
					end
				else
					a_error.set_file_path_not_valid
				end
			else
				a_error.set_argument_count_error
			end
		end

	close_file(a_error:INTERPRETER_ERROR; a_arguments:CHAIN[detachable ANY])
			-- Close the {FILE} specified in `a_arguments'.at(1).
			-- Set errors in `a_error' if an error occured
			-- Warning: This function is not pure. `a_error' may be modified
		do
			if a_arguments.count = 1 and then attached a_arguments.at (1) as la_argument then
				if attached {FILE}la_argument as la_file then
					la_file.close
				else
					a_error.set_arguments_not_valid
				end
			else
				a_error.set_argument_count_error
			end
		end

end
